//=============================================================================
// Code Atelier RPG Maker MZ - Gathering Point
//=============================================================================

/*:
 * @target MZ
 * @plugindesc Allows you to create gathering point events quickly.
 * @author Zecchan Silverlake
 *
 * @help CAMZ_GatheringPoint.js
 *
 * @command gatherpoint
 * @text Gathering Point
 * @desc Add conditions to this event as a gathering point
 *
 * @arg resources
 * @type struct<ResourceStruct>[]
 * @text Resources
 * @desc Resources that can be gathered
 * 
 * @arg message
 * @type string
 * @text Gathering Message
 * @desc The message that shown when the player access the gathering point
 * @default Do you want to gather item here?
 * 
 * @arg apcost
 * @type number
 * @text AP Cost
 * @desc The AP cost when the player do the gathering
 * @default 1
 *
 * @arg daymin
 * @type number
 * @text Recover Min
 * @desc The minimum day to recover this resource. 0 = does not deplete
 * @default 1
 *
 * @arg daymax
 * @type number
 * @text Recover Max
 * @desc The maximum day to recover this resource. 0 = does not deplete
 * @default 1
 *
 */

/*~struct~ResourceStruct:
 * @param type
 * @type string
 * @text Type
 * @desc The type of the item (item | armor | weapon)
 * @default item
 *
 * @param id
 * @type number
 * @text ID
 * @desc The ID of the item (number)
 *
 * @param chance
 * @type number
 * @text Chance
 * @desc The chance that this resource will be gained (percent)
 * @default 100
 *
 * @param qtymin
 * @type number
 * @text Min Quantity
 * @desc The minimum quantity that will be gained
 * @default 1
 *
 * @param qtymax
 * @type number
 * @text Max Quantity
 * @desc The maximum quantity that will be gained
 * @default 1
 *
 * @param switch
 * @type number
 * @text Switch ID
 * @desc This resource can only be gained if the switch ID is on. 0 = always able to gain
 * @default 0
 * 
 * @param tooltype
 * @type string
 * @text Tool Type
 * @desc The type of the tool needed to gather this item (item | armor | weapon)
 * @default item
 *
 * @param toolid
 * @type number
 * @text ID
 * @desc The ID of the tool needed to gather this item (number). 0 = always able to gain
 *
 * @param toolchance
 * @type number
 * @text Tool Break Chance
 * @desc The chance that the tool will break (percentage)
 * @default 0
*/