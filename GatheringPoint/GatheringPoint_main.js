
if (ca) {
    ca.OnGameSave.add(function(data) {
        data.gp = ca.gp.data;
    });

    ca.gp = {
        data: {
            availability: {}
        }
    };

    function deepCopy(s, d) {
        if (typeof s !== "object" || typeof d !== "object" || !s || !d) return;
        if (Array.isArray(s) && Array.isArray(d)) {
            d.splice(0, d.length);
            for(var i = 0; i < s.length; i++) d.push(s[i]);
        }
        else {
            for(var k in s) {
                if (typeof s[k] === "object" && typeof d[k] === "object")
                    deepCopy(s[k], d[k]);
                else
                    d[k] = s[k];
            }
        }
    }
    
    ca.OnGameLoad.add(function(data) {
        deepCopy(data.gp, ca.gp.data);
        mz.refreshEvents();
    });

    PluginManager.registerCommand("CAMZ_GatheringPoint", "setunavailable", args => {
        ca.gp.data.availability[args.id] = Number(args.day);
    });

    ca.OnCheckEventPage.addAfter(function (page) {
        if (this._lastReturn === false) return false;
    
        var c = page.list.where(c => c.code == 357 && c.parameters[0] == "CAMZ_GatheringPoint" && c.parameters[1] == "gatherpoint").first();
    
        if (!c) return true;
        var p = JSON.parse(c.parameters[3].resources).select(x => JSON.parse(x));
        var apcost = Number(c.parameters[3].apcost) || 1;
        var evt = this._this;
        var uid = "gp" + evt._mapId + "_" + evt._eventId;
        var avail = !km.data.gpAvailability[uid] || km.data.gpAvailability[uid] <= ca.cal.Clock.Day;
    
        var a = [];
        a.push(c);
        a.push({code:101,indent:0,parameters:["",0,0,2,""]});
        
        a.push({code:401,indent:0,parameters:[
            c.parameters[3].message
        ]});
        a.push({code:401,indent:0,parameters:[
            "(Cost: " + apcost + " AP)"
        ]});
        a.push({code:102,indent:0,parameters:[["Yes", "No"],1,0,2,0]});
    
        a.push({code:402,indent:0,parameters:[0, "Yes"]});
        a.push({code:0,indent:1,parameters:[]});
    
        a.push({code:402,indent:0,parameters:[1, "No"]});
        a.push({code:115,indent:1,parameters:[]});
        a.push({code:0,indent:1,parameters:[]});
    
        a.push({code:404,indent:0,parameters:[]});
    
        var dn = Number(c.parameters[3].daymin) || 0;
        var dx = Number(c.parameters[3].daymin) || 0;
        var avadate = km.data.gpAvailability[uid] || ca.cal.Clock.Day;
        if (dx >= dn && dn > 0 && dx > 0) {
            var ran = Math.round(Math.random() * (dx - dn) + dn);
            avadate = ca.cal.Clock.Day + ran;
        }   
    
        var gain = 0;
        var toolsToBreak = [];
        for(var i = 0; i < p.length; i++) {
            var ii = p[i];
            var swid = Number(ii.switch);
            if (swid && !mz.s(swid)) continue;
    
            var tt = ii.tooltype == "armor" ? "a" : ii.tooltype == "weapon" ? "w" : "i";
            var ti = Number(ii.toolid);
            var tc = Number(ii.toolchance) || 0;
            if ($gameParty && ti) {
                if (tt == "a" && !$gameParty.hasItem($dataArmors[ti], true)) continue;
                if (tt == "w" && !$gameParty.hasItem($dataWeapons[ti], true)) continue;
                if (tt == "i" && !$gameParty.hasItem($dataItems[ti], true)) continue;
                var tb = Math.random() * 100 < tc;
                if (tb) {
                    if (toolsToBreak.where(x => x.typ == tt && id == ti).length == 0)
                    toolsToBreak.push(
                        {
                            typ: tt,
                            id: ti
                        }
                    );
                }
            }
    
            var typ = ii.type == "armor" ? "a" : ii.type == "weapon" ? "w" : "i";
    
            var mn = Number(ii.qtymin) || 1;
            var mx = Number(ii.qtymax) || 1;
            var qty = Math.round(Math.random() * Math.abs(mx - mn) + Math.min(mn, mx));
    
            var getch = Math.random() * 100 < (Number(ii.chance) || 100);
    
            if (qty > 0 && getch) {
                a.push({code:101,indent:0,parameters:["", 0, 0, 2, ""]});        
                a.push({code:401,indent:0,parameters:[
                    "You got \\i" + typ + "[" + ii.id + "] x" + qty + "."
                ]});
                if (Number(ii.id)) {
                    if (typ == "i")
                        a.push({code:126,indent:0,parameters:[Number(ii.id), 0, 0, qty]});
                    if (typ == "w")
                        a.push({code:127,indent:0,parameters:[Number(ii.id), 0, 0, qty]});
                    if (typ == "a")
                        a.push({code:128,indent:0,parameters:[Number(ii.id), 0, 0, qty]});
                }
                gain++;
            }
        }
        
        if (gain == 0) {
            a.push({code:101,indent:0,parameters:["", 0, 0, 2, ""]});        
            a.push({code:401,indent:0,parameters:[
                "You got nothing."
            ]});
        }
    
        a.push({code:357,indent:0,parameters:["CAMZ_Calendar", "consumeap", "Consume AP", {amount: apcost}]});
        a.push({code:357,indent:0,parameters:["CAMZ_GatheringPoint", "setunavailable", "Set Unavailable", {id: uid, day: avadate}]});
        for(var i = 0; i < toolsToBreak.length; i++) {
            var t = toolsToBreak[i];
            if (t && $gameParty)
            if (t.typ == "i")
                a.push({code:126,indent:0,parameters:[Number(t.id), 1, 0, qty]});
            if (t.typ == "w")
                a.push({code:127,indent:0,parameters:[Number(t.id), 1, 0, qty]});
            if (t.typ == "a")
                a.push({code:128,indent:0,parameters:[Number(t.id), 1, 0, qty]});
            a.push({code:101,indent:0,parameters:["", 0, 0, 2, ""]});        
            a.push({code:401,indent:0,parameters:[
                "\\i" + t.typ + "[" + t.id + "] has broken!"
            ]});
        }
        a.push({code:657,indent:0,parameters:["Amount = " + apcost]});
    
        a.push({code:0,indent:0,parameters:[]});
        page.list = a;
        return avail;
    }.bind(ca.OnCheckEventPage));
}