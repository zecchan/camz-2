// === Event Handler ===
class EventHandler {
    constructor() {
        this._handlers = [];
    }

    add(f) {
        if (typeof f === "function")
            this._handlers.push(f);
    }

    addFirst(f) {
        if (typeof f === "function")
            this._handlers.splice(0, 0, f);
    }

    trigger(args, thisArg) {
        thisArg = thisArg || this;
        if (args && args.length > 0) {
            var a = [];
            for (var i = 0; i < args.length; i++)
                a.push(args[i]);
            args = a;
        } else
            args = [args];
        this._lastReturn = undefined;
        this._break = false;
        for (var i = 0; i < this._handlers.length; i++) {
            this.precall(thisArg, args);
            this._lastReturn = this._handlers[i].apply(thisArg, args);
            this.postcall(this._lastReturn, thisArg, args);
            if (this._break) return this._lastReturn;
        }
        return this._lastReturn;
    }

    precall(t, a) { }
    postcall(r, t, a) { }
}

// === Func Override ===
class FuncOverride {
    constructor(base, fname, defpos) {
        this._origin = base[fname];

        base["_" + fname] = this;
        base[fname] = function () {
            return this["_" + fname].exec(this, arguments);
        };

        this.before = [];
        this.after = [];
        this._defpos = defpos || "after";
    }

    callOrigin = true;

    addBefore(f) {
        this.before.splice(0, 0, f);
    }

    addAfter(f) {
        this.after.push(f);
    }

    add(f) {
        if (this._defpos == "before")
            this.before.push(f);
        else
            this.after.push(f);
    }

    exec(thisArg, args) {
        thisArg = thisArg || this;
        if (args && args.length > 0) {
            var a = [];
            for (var i = 0; i < args.length; i++)
                a.push(args[i]);
            args = a;
        } else
            args = [args];
        this._lastReturn = undefined;
        this._break = false;
        this._this = thisArg;

        for (var i = 0; i < this.before.length; i++) {
            this.precall(thisArg, args);
            this._lastReturn = this.before[i].apply(thisArg, args);
            this.postcall(this._lastReturn, thisArg, args);
            if (this._break) return this._lastReturn;
        }

        if (this.callOrigin) {
            this.precall(thisArg, args);
            this._lastReturn = this._origin.apply(thisArg, args);
            this.postcall(this._lastReturn, thisArg, args);
        }

        for (var i = 0; i < this.after.length; i++) {
            this.precall(thisArg, args);
            this._lastReturn = this.after[i].apply(thisArg, args);
            this.postcall(this._lastReturn, thisArg, args);
            if (this._break) return this._lastReturn;
        }
        return this._lastReturn;
    }

    precall(t, a) { }
    postcall(r, t, a) { }
}