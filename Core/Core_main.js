class CAMZ {
    static Version = [2, 0];
    checkVersion(ver) {
        if (ver[0] > CAMZ.Version[0]) return false;
        if (ver[0] == CAMZ.Version[0] && ver[1] > CAMZ.Version[1]) return false;
        return true;
    };

    constructor() {
        this.eval = new CAEvaluator();
        this.img = new CAImage(this);
        this.jss = new JSONSSDictionary();
        this.initialize();
    };

    initialize() {
        if (this._initialized) return;
        this._initialized = true;

        for(var i = 0; i < mz.plugins.CAMZ.length; i++) {
            var jsonss = mz.plugins.CAMZ[i].JSONSS;
            this.jss.parse(jsonss);
        }

        this.initEvents();
    };

    initEvents() {
        
    }
}

PluginManager.registerCommand("CAMZ", "evalbool", args => {
    var swID = Number(args.switch) || 0;
    var script = args.switch;

    if (swID) {
        ca.s(swID, ca.eval.exec(script));
    }
});

PluginManager.registerCommand("CAMZ", "showimage", args => {
    var img = args.image;
    var idx = Number(args.index) || 1;
    var sty = args.style;
    var cls = args.jssclass;
    if (typeof cls === "string") cls = cls.split(" ").where(x => x.trim() != "");
    else cls = [];
    if (typeof sty === "string") {
        if (sty.trim() == "") sty = null;
    }
    
    ca.img.showPicture(idx, img, cls, sty);
});