// === CA Events ===
CAMZ.prototype.initEvents = function() {
    this.OnGameSave = new FuncOverride(DataManager, "makeSaveContents");
    this.OnGameSave.postcall = function (r, t, a) {
        if (Array.isArray(a)) {
            if (r) {
                while (a.length > 0) a.pop();
                a.push(r);
            } else {
                this._lastReturn = a[0];
            }
        }
    }
    this.OnGameLoad = new FuncOverride(DataManager, "extractSaveContents");
    this.OnConvertEscapeCharacters = new FuncOverride(Window_Base.prototype, "convertEscapeCharacters");
    this.OnConvertEscapeCharacters.postcall = function (r, t, a) {
        if (r) a[0] = r;
    };
    this.OnCheckEquip = new FuncOverride(Game_BattlerBase.prototype, "canEquip");
    this.OnCheckEventPage = new FuncOverride(Game_Event.prototype, "meetsConditions");
    this.OnCheckEventPage.LocalVars = {};
    this.OnMapStart = new FuncOverride(Scene_Map.prototype, "start");
    this.OnMapStop = new FuncOverride(Scene_Map.prototype, "stop");
    this.OnMapCreateAllWindows = new FuncOverride(Scene_Map.prototype, "createAllWindows", "before");
    this.OnMapUpdate = new FuncOverride(Scene_Map.prototype, "update", "after");
    this.OnNewGame = new FuncOverride(DataManager, "setupNewGame");
    this.OnGameMessageSetSpeakerName = new FuncOverride(Game_Message.prototype, "setSpeakerName");

    // add internal triggers
    this.OnConvertEscapeCharacters.addBefore(function (text) {
        text = text.replace(/\\/g, '\x1b');
        text = text.replace(/\x1bii\[([0-9]+)\]/gi, function (match, id) {
            var item = $dataItems[id];
            if (item)
                return "\\i[" + item.iconIndex + "] \x1bc[blue]" + item.name + "\x1bc[0]";
            return match;
        });
        text = text.replace(/\x1bia\[([0-9]+)\]/gi, function (match, id) {
            var item = $dataArmors[id];
            if (item)
                return "\\i[" + item.iconIndex + "] \x1bc[blue]" + item.name + "\x1bc[0]";
            return match;
        });
        text = text.replace(/\x1biw\[([0-9]+)\]/gi, function (match, id) {
            var item = $dataWeapons[id];
            if (item)
                return "\\i[" + item.iconIndex + "] \x1bc[blue]" + item.name + "\x1bc[0]";
            return match;
        });
        var colors = mz.Colors;
        text = text.replace(/\x1bC\[(\w+)\]/gi, function (match, color) {
            color = colors[color] || color;
            return "\\c[" + (color || 0) + "]";
        });

        return text;
    }.bind(this));

    this.OnCheckEventPage.addAfter(function (page) {
        if (this._lastReturn === false) return false;

        var cc = page.list.where(c => c.code == 357 && c.parameters[0] == "CAMZ" && c.parameters[1] == "conditions");

        for (var i = 0; i < cc.length; i++) {
            var c = cc[i];
            var val = ca.eval.exec(c.parameters[3].script);
            if (!val) return false;
        }

        return true;
    }.bind(this.OnCheckEventPage));

    this.OnCheckEquip.add(function (item) {
        if (!this._lastReturn) return false;
        var valid = this._lastReturn;
        if (item.meta) {
            var aid = this._this.actorId();
            if (item.meta.whitelist) {
                valid = false;
                var spl = item.meta.whitelist.split(",");
                var found = false;
                for (var i = 0; i < spl.length; i++) {
                    if (aid == Number(spl[i]))
                        found = true;
                }
                if (found) valid = true;
            }
            if (item.meta.blacklist) {
                valid = false;
                var spl = item.meta.blacklist.split(",");
                var found = false;
                for (var i = 0; i < spl.length; i++) {
                    if (aid == Number(spl[i]))
                        found = true;
                }
                if (!found) valid = true;
            }
        }
        return valid;
    }.bind(this.OnCheckEquip));
};