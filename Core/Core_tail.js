// global functions
function deepClone(o) {
    if (typeof o !== "object") return o;
    var cln = {};
    if (Array.isArray(o)) {
        cln = [];
        for(var i = 0; i < o.length; i++) {
            cln.push(deepClone(o));
        }
    } else {
        for(var k in o) {
            cln[k] = deepClone(o[k]);
        }
    }
    return cln;
}

function intval(value, defaultValue) {
    return !isNaN(Number(value)) ? Number(value) : (Number(defaultValue) || 0);
}

// Extensions
Game_Party.prototype.maxItems = function(item)
{
    if (item && item.meta && item.meta.max)
        return Number(item.meta.max) || 99;
    return 99;
}

// Off we go!
var ca = new CAMZ();
console.log("CAMZ v" + CAMZ.Version.join("."));