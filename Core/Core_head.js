//=============================================================================
// Code Atelier RPG Maker MZ - Core
//=============================================================================

/*:
 * @target MZ
 * @plugindesc Contains core functionality for CAMZ Plugins.
 * @author Zecchan Silverlake
 *
 * @help CAMZ.js
 *
 * This plugin contains necessary logic for CAMZ Plugins.
 * 
 * CAMZ Events:
 * 
 * OnGameSave(data)
 * OnGameLoad(data)
 * OnConvertEscapeCharacters(text) return string
 * OnCheckEquip(item) return bool
 * OnInit()
 *
 * @command conditions
 * @text Add Conditions
 * @desc Add more condition for this Event using JavaScript.
 * (Multiple instances will be joined by AND operator)
 *
 * @arg script
 * @type multiline_string
 * @text Script
 * @desc JavaScript that evaluates to boolean.
 *
 * @command evalbool
 * @text Evaluate to Switch
 * @desc Evaluate JavaScript expression to Switch's ON/OFF
 *
 * @arg script
 * @type multiline_string
 * @text Script
 * @desc JavaScript that evaluates to boolean.
 *
 * @arg switch
 * @type number
 * @text Switch ID
 * @default 1
 * @desc The switch ID to return the value to.
 *
 * @command showimage
 * @text Show Image
 * @desc Shows an image using JSON StyleSheet
 *
 * @arg index
 * @type number
 * @text Picture Index
 * @default 1
 * @desc The picture index.
 *
 * @arg image
 * @type file
 * @text Image
 * @require 1
 * @dir img/pictures
 * @desc The image to show.
 * 
 * @arg jssclass
 * @type string
 * @text JSS Classes
 * @desc JSON StyleSheet classes
 * 
 * @arg style
 * @type multiline_string
 * @text Custom Style
 * @desc JSON StyleSheet (don't add braces)
 * 
 * @param JSONSS
 * @text JSON StyleSheet
 * @type multiline_string
 * @desc Enter custom JSON StyleSheet here.
 * @default []
 */

class MZ {
    constructor() {
        this.initPlugins();
    }

    initPlugins() {
        this.plugins = {};
        if ($plugins) {
            for(var i = 0; i < $plugins.length; i++) {
                var p = $plugins[i];
                this.plugins[p.name] = this.plugins[p.name] || [];
                this.plugins[p.name].push(p.parameters);
            }
        }
    }

    v(id, val) {
        if (val !== undefined)
            return $gameVariables.setValue(id, val);
        return $gameVariables.value(id);
    };

    s(id, val) {
        if (val !== undefined)
            return $gameSwitches.setValue(id, val);
        return $gameSwitches.value(id);
    };

    partySize() {
        return $gameParty._actors.length;
    };

    refreshEvents() {
        if ($gameMap)
            $gameMap.requestRefresh();
    }

    refreshPlayer() {
        if ($gamePlayer)
            $gamePlayer.refresh();
    }

    execCommonEvent(id) {
        $gameTemp?.reserveCommonEvent(id);
    }

    gameOver() {
        SceneManager.goto(Scene_Gameover);
    }

    Colors = {
        "white": 0,
        "lightblue": 1,
        "lightred": 2,
        "lightgreen": 3,
        "lightteal": 4,
        "lightpurple": 5,
        "lightyellow": 6,
        "gray": 7,
        "lightgray": 8,
        "blue": 9,
        "red": 10,
        "green": 11,
        "teal": 12,
        "purple": 13,
        "yellow": 14,
        "black": 15,
        "purpleblue": 16,
        "pureyellow": 17,
        "crimson": 18,
        "lightblack": 19,
        "orange": 20,
        "lightorange": 21,
        "skyblue": 22,
        "lightskyblue": 23,
        "lime": 24,
        "brown": 25,
        "lightbrown": 25,
        "deeppurple": 26,
        "fuchsia": 27,
        "grassgreen": 28,
        "lightgrassgreen": 29,
        "magenta": 30,
        "lightmagenta": 31
    };
}

var mz = new MZ();