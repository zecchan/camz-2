// === CAImage ===
class CAImage {
    constructor(camz) {
        this.ca = camz;
    }

    loadBitmap(path, smooth) {
        var bitmap = ImageManager.loadBitmap("img/pictures/", path);
        bitmap.smooth = smooth !== false;
        return new Promise(function (resolve) {
            if (bitmap.isReady())
                resolve(bitmap);
            bitmap.addLoadListener(function () { resolve(bitmap); });
        });
    };

    showPicture(id, path, cls, customjss) {
        if (!$gameScreen) return;
        var bitmapPath = path;
        var realPictureId = $gameScreen.realPictureId(id);
        var picture = new Game_Picture();
        picture.show(path, 0, 0, 0, 0, 0, 0, 0);
        $gameScreen._pictures[realPictureId] = picture;

        if (!Array.isArray(cls)) {
            if (cls)
                cls = [cls];
            else
                cls = [];
        }
        picture._style = this.ca.jss.compile.apply(this.ca.jss, cls);   
        if (typeof customjss === "string") {
            var jss = this.ca.jss.parseStyle(customjss);
            picture._style = picture._style.merge(jss);
        }     

        this.loadBitmap(bitmapPath).then(function (bitmap) {
            if (this._style && typeof this._style.applyStyle == "function")
                this._style.applyStyle(this, bitmap);
        }.bind(picture));
    };

    clearPicture(id) {
        if (!$gameScreen) return;
        if (Array.isArray(id)) {
            for (var i = 0; i < id.length; i++)
                this.clearPicture(id[i]);
            return;
        }
        if (id) {
            const realPictureId = $gameScreen.realPictureId(id);
            $gameScreen._pictures[realPictureId] = null;
        } else {
            for (var i = 1; i < $gameScreen._pictures.length; i++)
                $gameScreen._pictures[i] = null;
        }
    }
}