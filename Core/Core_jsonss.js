// === JSON Stylesheet ===
class JSONSSDictionary {
    constructor() {
        this.dictionary = {};
    }

    parse(ss) {
        if (typeof ss === "string")
            ss = JSON.parse(ss);
        var res = [];
        if (!Array.isArray(ss))
            return res;
        for(var i = 0; i < ss.length; i++) {
            var cls = ss[i];
            if (typeof cls !== "object") continue;
            var s = this.parseStyle(cls, true);
            if (s && s.class && typeof s.class == "string") {
                if (this.dictionary[s.class]) {
                    this.dictionary[s.class] = this.dictionary[s.class].merge(s);
                } else {
                    this.dictionary[s.class] = new JSONSS(s);
                }
            }
            if (s)
                res.push(s);
        }
        return res;
    }

    parseStyle(sty, isClass) {
        if (typeof sty === "string")
            sty = JSON.parse("{" + sty + "}");
        if (typeof sty !== "object")
            return null;
        if (!isClass)
            delete sty.class;
        return isClass ? sty : new JSONSS(sty);
    }

    compileClass(cls) {
        if (typeof cls !== "string") return null;
        return this.dictionary[cls];
    }

    compile() {
        var res = new JSONSS();
        for(var i = 0; i < arguments.length; i++) {
            var ar = arguments[i];
            if (typeof ar !== "string") continue;
            var o = this.compileClass(ar);
            if (o) res = res.merge(o);
        }
        return res;
    }
}

class JSONSS {
    _isJSONSS = true;
    constructor(style) {
        if (typeof style !== "object")
            style = {};
        if (style._isJSONSS)
            this.style = style.clone().style;
        else
            this.style = style;

        // merge key alias
        this.cleanse();
    }

    cleanse() {
        var s = this.style;
        s.ha = s.ha || s.hAlign; delete s.hAlign;
        s.va = s.va || s.vAlign; delete s.vAlign;
        s.d = s.d || s.display; delete s.display;
        s.l = !isNaN(Number(s.l)) ? Number(s.l) : Number(s.left) || undefined; delete s.left;
        s.t = !isNaN(Number(s.t)) ? Number(s.t) : Number(s.top) || undefined; delete s.top;
        s.r = !isNaN(Number(s.r)) ? Number(s.r) : Number(s.right) || undefined; delete s.right;
        s.b = !isNaN(Number(s.b)) ? Number(s.b) : Number(s.bottom) || undefined; delete s.bottom;
        s.ml = !isNaN(Number(s.ml)) ? Number(s.ml) : Number(s.marginLeft) || undefined; delete s.marginLeft;
        s.mt = !isNaN(Number(s.mt)) ? Number(s.mt) : Number(s.marginTop) || undefined; delete s.marginTop;
        s.mr = !isNaN(Number(s.mr)) ? Number(s.mr) : Number(s.marginRight) || undefined; delete s.marginRight;
        s.mb = !isNaN(Number(s.mb)) ? Number(s.mb) : Number(s.marginBottom) || undefined; delete s.marginBottom;
        s.m = !isNaN(Number(s.m)) ? Number(s.m) : Number(s.margin) || undefined; delete s.margin;
        s.a = !isNaN(Number(s.a)) ? Number(s.a) : Number(s.alpha) || undefined; delete s.alpha;
        s.bl = s.bl || s.blend; delete s.blend;

        s.h = !isNaN(Number(s.h)) ? Number(s.h) : Number(s.height) || undefined; delete s.height;
        s.w = !isNaN(Number(s.w)) ? Number(s.w) : Number(s.width) || undefined; delete s.width;
        s.nh = !isNaN(Number(s.nh)) ? Number(s.nh) : Number(s.minHeight) || undefined; delete s.minHeight;
        s.xh = !isNaN(Number(s.xh)) ? Number(s.xh) : Number(s.maxHeight) || undefined; delete s.maxHeight;
        s.nw = !isNaN(Number(s.nw)) ? Number(s.nw) : Number(s.minWidth) || undefined; delete s.minWidth;
        s.xw = !isNaN(Number(s.xw)) ? Number(s.xw) : Number(s.maxWidth) || undefined; delete s.maxWidth;

        s.f = s.f || s.flip;
    }

    merge(style) {
        if (typeof style !== "object") return this.clone();
        if (style._isJSONSS) style = style.style;
        var o = this.clone();
        o.cleanse();
        for(var k in style) {
            if (style[k] !== undefined)
                o.style[k] = style[k];
        }
        o.cleanse();
        return o;
    }

    clone() {
        var s = deepClone(this.style);
        return new JSONSS(s);
    }

    applyStyle(picture, bmp) {
        this.cleanse();
        var s = this.style;
        // initial values
        var hAl = {'left': 'l', 'l': 'l', 'center': 'c', 'c': 'c', 'right': 'r', 'r': 'r', 'stretch': 's', 's': 's'};
        var ha = hAl[s.ha] || 'l';
        var vAl = {'top': 't', 't': 't', 'middle': 'm', 'm': 'm', 'bottom': 'b', 'b': 'b', 'stretch': 's', 's': 's'};
        var va = vAl[s.va] || 't';
        var dis = {'absolute': 'a', 'a': 'a', 'stretch': 's', 's': 's', 'fill': 'f', 'f': 'f', 'fit': 't', 't': 't'}
        var d = dis[s.d] || 'a';

        var x = 0;
        var y = 0;

        bmp = bmp || ImageManager.loadPicture(picture._name);
        var gw = Graphics.width;
        var gh = Graphics.height;
        var sw = bmp.width;
        var sh = bmp.height;
        var dw = sw;
        var dh = sh;

        var ml = Number(s.ml || s.m || 0) || 0;
        var mt = Number(s.mt || s.m || 0) || 0;
        var mr = Number(s.mr || s.m || 0) || 0;
        var mb = Number(s.mb || s.m || 0) || 0;

        var a = Math.min(255, Math.max(0, (Number(s.a) || 100) / 100 * 255));

        if (d == 's') {
            // stretch to screen
            dw = gw;
            dh = gh;
            x = (gw - dw) / 2 + ml - mr;
            y = (gh - dh) / 2 + mt - mb;
        }
        else if (d == 'f') {
            // fill the screen
            var sc = Math.max(gw / sw, gh / sh);
            dw = sw * sc;
            dh = sh * sc;
            x = (gw - dw) / 2 + ml - mr;
            y = (gh - dh) / 2 + mt - mb;
            console.log(gw, sw, gw / sw, gh, sh, gh / sh, sc);
        }
        else if (d == 't') {
            // fit inside the screen
            var sc = Math.min(gw / sw, gh / sh);
            dw = sw * sc;
            dh = sh * sc;
            x = (gw - dw) / 2 + ml - mr;
            y = (gh - dh) / 2 + mt - mb;
        }
        else {
            // absolute layouting
            if (s.w !== undefined) dw = s.w; 
            if (dw < s.nw) dw = Number(s.nw) || dw;
            if (dw > s.xw) dw = Number(s.xw) || dw;
            var wscl = dw / sw;

            if (s.h !== undefined) dh = s.h;
            else dh = sh * wscl;
            if (dh < s.nh) dh = Number(s.nh) || dh;
            if (dh > s.xh) dh = Number(s.xh) || dh;
            var hscl = dh / sh;

            if (s.w === undefined) {
                var ddw = sw * hscl;
                if (ddw < s.nw) ddw = Number(s.nw) || ddw;
                if (ddw > s.xw) ddw = Number(s.xw) || ddw;
                dw = ddw;
            }

            if (ha == 'c') x = (gw - dw) / 2;
            if (ha == 'r') x = gw - dw - mr;
            if (ha == 'l') x = ml;
            if (ha == 's') {
                dw = gw;
                x = (gw - dw) / 2 + ml - mr;
            }
            if (va == 'm') y = (gh - dh) / 2;
            if (va == 'b') y = gh - dh - mb;
            if (va == 't') y = mt;
            if (va == 's') {
                dh = gh;
                y = (gh - dh) / 2 + mt - mb;
            }
        }

        if (typeof s.f === "string") {
            if (s.f.includes("h")) {   
                x += dw;
                dw *= -1;
            }
            if (s.f.includes("v")) {
                y += dh;
                dh *= -1;
            }
        }

        var opt = {
            origin: 0,
            x: x,
            y: y,
            scaleX: dw / sw * 100,
            scaleY: dh / sh * 100,
            opacity: a,
            blend: s.bl == "multiply" ? 1 : 0
        };
        console.log(opt, dw, dh);        
        picture.show(picture._name, opt.origin, opt.x, opt.y, opt.scaleX, opt.scaleY, opt.opacity, opt.blend);
    }
}

Game_Picture.prototype.applyStyle = function (jsonss) {
    if (typeof jsonss !== "object" || !jsonss._isJSONSS) return;
    jsonss.applyStyle(this);
}