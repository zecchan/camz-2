// === Evaluator ===
class CAEvaluator {
    constructor() {
        this.Args = {};
    }

    exec(script) {
        var keys = [];
        var values = [];
        for (var k in this.Args) {
            keys.push(k);
            values.push(this.Args[k]);
        }
        var js = "return " + script;
        var f = new Function(keys, js);
        try{
            var val = f.apply(this.Args, values);
            if (!val) return false;
        }
        catch(ex) {
            console.error(ex);
            return false;
        }
        return true;
    }
}