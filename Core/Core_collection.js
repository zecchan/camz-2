// === Collection Functions ===
Array.prototype.where = function(f) {
    if (typeof f !== "function") return [];
    var res = [];
    for(var i = 0; i < this.length; i++) {
        if (f.apply(this, [this[i], i])) {
            res.push(this[i]);
        }
    }
    return res;
}

Array.prototype.select = function(f) {
    if (typeof f !== "function") return [];
    var res = [];
    for(var i = 0; i < this.length; i++) {
        var s = f.apply(this, [this[i], i]);
        res.push(s);
    }
    return res;
}

Array.prototype.selectMany = function(f) {
    if (typeof f !== "function") return [];
    var res = [];
    for(var i = 0; i < this.length; i++) {
        var s = f.apply(this, [this[i], i]);
        if (Array.isArray(s)) {
            for(var x = 0; x < s.length; x++)
                res.push(s[x]);
        }
    }
    return res;
}

Array.prototype.sum = function(f) {
    var res = 0;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (typeof f === "function")
            s = f.apply(this, [s, i]);
        res += s;
    }
    return res;
}

Array.prototype.max = function(f) {
    var res = null;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (typeof f === "function")
            s = f.apply(this, [s, i]);
        if (res == null) res = s;
        else res = Math.max(res, s);
    }
    return res;
}

Array.prototype.min = function(f) {
    var res = null;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (typeof f === "function")
            s = f.apply(this, [s, i]);
        if (res == null) res = s;
        else res = Math.min(res, s);
    }
    return res;
}

Array.prototype.avg = function(f) {
    var res = 0;
    var cnt = 0;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (typeof f === "function")
            s = f.apply(this, [s, i]);
        res += s;
        cnt++;
    }
    return res / cnt;
}

Array.prototype.first = function() {
    return this[0];
}

Array.prototype.firstOrDefault = function(def) {
    if (this.length == 0) return def;
    return this[0];
}

Array.prototype.last = function() {
    return this[this.length - 1];
}

Array.prototype.lastOrDefault = function(def) {
    if (this.length == 0) return def;
    return this[this.length - 1];
}

Array.prototype.count = function(f) {
    if (typeof f !== "function") return this.length;
    var cnt = 0;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (f.apply(this, [s, i]))
        cnt++;
    }
    return cnt;
}

Array.prototype.any = function(f) {
    if (typeof f !== "function") return this.length > 0;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (f.apply(this, [s, i]))
            return true;
    }
    return false;
}

Array.prototype.pushRange = function(r) {
    if (!Array.isArray(r)) return this;
    for(var i = 0; i < r.length; i++)
        this.push(r[i]);
    return this;
}