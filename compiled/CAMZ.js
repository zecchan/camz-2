//=============================================================================
// Code Atelier RPG Maker MZ - Core
//=============================================================================
// === Collection Functions ===
Array.prototype.where = function(f) {
    if (typeof f !== "function") return [];
    var res = [];
    for(var i = 0; i < this.length; i++) {
        if (f.apply(this, [this[i], i])) {
            res.push(this[i]);
        }
    }
    return res;
}

Array.prototype.select = function(f) {
    if (typeof f !== "function") return [];
    var res = [];
    for(var i = 0; i < this.length; i++) {
        var s = f.apply(this, [this[i], i]);
        res.push(s);
    }
    return res;
}

Array.prototype.selectMany = function(f) {
    if (typeof f !== "function") return [];
    var res = [];
    for(var i = 0; i < this.length; i++) {
        var s = f.apply(this, [this[i], i]);
        if (Array.isArray(s)) {
            for(var x = 0; x < s.length; x++)
                res.push(s[x]);
        }
    }
    return res;
}

Array.prototype.sum = function(f) {
    var res = 0;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (typeof f === "function")
            s = f.apply(this, [s, i]);
        res += s;
    }
    return res;
}

Array.prototype.avg = function(f) {
    var res = 0;
    var cnt = 0;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (typeof f === "function")
            s = f.apply(this, [s, i]);
        res += s;
        cnt++;
    }
    return res / cnt;
}

Array.prototype.first = function() {
    return this[0];
}

Array.prototype.firstOrDefault = function(def) {
    if (this.length == 0) return def;
    return this[0];
}

Array.prototype.last = function() {
    return this[this.length - 1];
}

Array.prototype.lastOrDefault = function(def) {
    if (this.length == 0) return def;
    return this[this.length - 1];
}

Array.prototype.count = function(f) {
    if (typeof f !== "function") return this.length;
    var cnt = 0;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (f.apply(this, [s, i]))
        cnt++;
    }
    return cnt;
}

Array.prototype.any = function(f) {
    if (typeof f !== "function") return this.length > 0;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (f.apply(this, [s, i]))
            return true;
    }
    return false;
}
// === JSON Stylesheet ===
class JSONSSDictionary {
    constructor() {
        this.dictionary = {};
    }

    parse(ss) {
        if (typeof ss === "string")
            ss = JSON.parse(ss);
        var res = [];
        if (!Array.isArray(ss))
            return res;
        for(var i = 0; i < ss.length; i++) {
            var cls = ss[i];
            if (typeof cls !== "object") continue;
            var s = this.parseStyle(cls, true);
            if (s && s.class && typeof s.class == "string") {
                if (this.dictionary[s.class]) {
                    this.dictionary[s.class] = this.dictionary[s.class].merge(s);
                } else {
                    this.dictionary[s.class] = new JSONSS(s);
                }
            }
            if (s)
                res.push(s);
        }
        return res;
    }

    parseStyle(sty, isClass) {
        if (typeof sty === "string")
            sty = JSON.parse("{" + sty + "}");
        if (typeof sty !== "object")
            return null;
        if (!isClass)
            delete sty.class;
        return isClass ? sty : new JSONSS(sty);
    }

    compileClass(cls) {
        if (typeof cls !== "string") return null;
        return this.dictionary[cls];
    }

    compile() {
        var res = new JSONSS();
        for(var i = 0; i < arguments.length; i++) {
            var ar = arguments[i];
            if (typeof ar !== "string") continue;
            var o = this.compileClass(ar);
            if (o) res = res.merge(o);
        }
        return res;
    }
}

class JSONSS {
    _isJSONSS = true;
    constructor(style) {
        if (typeof style !== "object")
            style = {};
        if (style._isJSONSS)
            this.style = style.clone().style;
        else
            this.style = style;

    }

    merge(style) {
        if (typeof style !== "object") return this.clone();
        var o = this.clone();
        for(var k in style) {
            o.style[k] = style[k];
        }
    }

    clone() {
        var s = deepClone(this.style);
        return new JSONSS(s);
    }
}
// global functions
function deepClone(o) {
    if (typeof o !== "object") return o;
    var cln = {};
    if (Array.isArray(o)) {
        cln = [];
        for(var i = 0; i < o.length; i++) {
            cln.push(deepClone(o));
        }
    } else {
        for(var k in o) {
            cln[k] = deepClone(o[k]);
        }
    }
    return cln;
}

// Off we go!
console.log("CAMZ v" + CAMZ.Version.join("."));
