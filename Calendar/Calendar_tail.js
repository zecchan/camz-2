if (ca) {
    ca.cal = new CAMV_Calendar();

    // auto read calendar data
    if (mz.plugins.CAMZ_Calendar[0]) {
        var p = mz.plugins.CAMZ_Calendar[0];
        var ts = p.timeStruct = JSON.parse(p.timeStruct);
        var ds = p.dateStruct = JSON.parse(p.dateStruct);

        ts.mode = ts.mode == "true" ? "phase" : "clock";
        ds.mode = ds.mode == "true" ? "day" : "dmy";

        ts.phases = JSON.parse(ts.phases).select(x => {
            var w = JSON.parse(x)
            w.indoorTint = JSON.parse(w.indoorTint);
            w.outdoorTint = JSON.parse(w.outdoorTint);
            return w;
        });
        ds.dows = JSON.parse(ds.dows).select(x => JSON.parse(x));
        ds.months = JSON.parse(ds.months).select(x => JSON.parse(x));

        ca.cal.TimeSystem = ts;
        ca.cal.DateSystem = ds;

        for(var i = 0; i < ca.cal.DateSystem.months.length; i++) {
            var m = ca.cal.DateSystem.months[i];
            m.rainchance = Number(m.rainchance) || 0;
            m.snowchance = Number(m.snowchance) || 0;
        }

        ca.cal.InitialDayOfWeek = Number(p.initDOW) || 0;
        ca.cal.Clock.DayOfWeek = Number(p.initDOW) || 0;
        ca.cal.Clock.Phase = Number(p.initPhase) || 0;
        ca.cal.Clock.Day = Number(p.initDay) || 1;
        ca.cal.Clock.Month = Number(p.initMonth) || 1;
        ca.cal.Clock.Year = Number(p.initYear) || 1;
        ca.cal.Clock.Second = Number(p.initSecond) || 0;
        ca.cal.Clock.Minute = Number(p.initMinute) || 0;
        ca.cal.Clock.Hour = Number(p.initHour) || 0;
        ca.cal.Clock.ActionPoint = Number(ts.maxap) || 5;
        ca.cal.Clock.normalize();

        // save/load data
        ca.OnGameSave.add(function (data) {
            data.Calendar = {
                maxAP: ca.cal.maxAP(),
                weather: ca.cal.CurrentWeather,
                clock: {
                    DayOfWeek: ca.cal.Clock.DayOfWeek,
                    Phase: ca.cal.Clock.Phase,
                    Day: ca.cal.Clock.Day,
                    Month: ca.cal.Clock.Month,
                    Year: ca.cal.Clock.Year,
                    Second: ca.cal.Clock.Second,
                    Minute: ca.cal.Clock.Minute,
                    Hour: ca.cal.Clock.Hour,
                    ActionPoint: ca.cal.Clock.ActionPoint
                }
            };
        });

        ca.OnGameLoad.add(function (data) {
            console.log(data.Calendar);
            if (data.Calendar) {
                var cal = data.Calendar;
                ca.cal.MaximumAP = intval(cal.maxAP, ca.cal.MaximumAP);
                ca.cal.Clock.DayOfWeek = intval(cal.clock.DayOfWeek, ca.cal.Clock.DayOfWeek);
                ca.cal.Clock.Phase = intval(cal.clock.Phase, ca.cal.Clock.Phase);
                ca.cal.Clock.Day = intval(cal.clock.Day, ca.cal.Clock.Day);
                ca.cal.Clock.Month = intval(cal.clock.Month, ca.cal.Clock.Month);
                ca.cal.Clock.Year = intval(cal.clock.Year, ca.cal.Clock.Year);
                ca.cal.Clock.Second = intval(cal.clock.Second, ca.cal.Clock.Second);
                ca.cal.Clock.Minute = intval(cal.clock.Minute, ca.cal.Clock.Minute);
                ca.cal.Clock.Hour = intval(cal.clock.Hour, ca.cal.Clock.Hour);
                ca.cal.Clock.ActionPoint = intval(cal.clock.ActionPoint, ca.cal.Clock.ActionPoint);
                ca.cal.CurrentWeather = cal.weather || "none";
            }
            ca.cal.updateEvalArgs();
            ca.cal.updateTint();
        });

        ca.OnMapStart.add(function () {
            ca.cal.updateEvalArgs();
            ca.cal.updateTint();
        });
        ca.OnCheckEventPage.addBefore(function () {
            ca.cal.updateEvalArgs();
            return true;
        });

        // Window add
        if (p.showMapWindow == "true") {
            ca.cal.MapWindowTopPrefix = p.mapTopPrefix;
            ca.cal.MapWindowTopFormat = p.mapTopFormat;
            ca.cal.MapWindowTopSuffix = p.mapTopSuffix;
            ca.cal.MapWindowBottomPrefix = p.mapBottomPrefix;
            ca.cal.MapWindowBottomFormat = p.mapBottomFormat;
            ca.cal.MapWindowBottomSuffix = p.mapBottomSuffix;
            // === UI ===
            ca.OnMapCreateAllWindows.addAfter(function () {
                const wx = 0;
                const wy = 0;
                const ww = 360;
                const wh = this.calcWindowHeight(2, false);
                var rect = new Rectangle(wx, wy, ww, wh);
                this._calendarWindow = new Window_Calendar(rect);
                this.addWindow(this._calendarWindow);
            });
            ca.OnMapUpdate.addAfter(function () {
                this._calendarWindow.refresh();
            });
        }
    }

    PluginManager.registerCommand("CAMZ_Calendar", "nextphase", args => {
        ca.cal.add("0P1");
    });
    PluginManager.registerCommand("CAMZ_Calendar", "nextday", args => {
        ca.cal.nextDay("0" + args.time);
    });
    PluginManager.registerCommand("CAMZ_Calendar", "addtime", args => {
        ca.cal.add(args.time);
    });
    PluginManager.registerCommand("CAMZ_Calendar", "subtime", args => {
        ca.cal.subtract(args.time);
    });
    PluginManager.registerCommand("CAMZ_Calendar", "consumeap", args => {
        ca.cal.consumeAP(Number(args.amount));
    });

    console.log("CAMZCalendar initialized");
}

//-----------------------------------------------------------------------------
// Window_Calendar
//
// The window for displaying the map name on the map screen.

function Window_Calendar() {
    this.initialize(...arguments);
}

Window_Calendar.prototype = Object.create(Window_Base.prototype);
Window_Calendar.prototype.constructor = Window_Calendar;

Window_Calendar.prototype.initialize = function (rect) {
    Window_Base.prototype.initialize.call(this, rect);
    this.opacity = 0;
    this.refresh();
};

Window_Calendar.prototype.refresh = function () {
    this.contents.clear();
    var ofs = this.contents.fontSize;
    this.contents.fontSize *= 1.5;
    this.contents.fontBold = true;
    this.contents.outlineColor = "white";
    this.contents.outlineWidth = 8;
    this.contents.textColor = "black";
    this.contents.drawText(ca.cal.MapWindowTopPrefix + ca.cal.Clock.toString(ca.cal.MapWindowTopFormat) + ca.cal.MapWindowTopSuffix, 6, 0, 240, this.lineHeight() * 1.1, "left");

    this.contents.fontSize = ofs * 0.7;
    this.contents.outlineColor = "black";
    this.contents.outlineWidth = 2;
    this.contents.textColor = "white";
    this.contents.fontBold = false;
    this.contents.drawText(ca.cal.MapWindowBottomPrefix + ca.cal.Clock.toString(ca.cal.MapWindowBottomFormat) + ca.cal.MapWindowBottomSuffix, 6, this.lineHeight() * 1.2, 160, this.lineHeight() * 0.5, "left");

    this.contents.fontSize = ofs;
};
