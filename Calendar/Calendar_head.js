//=============================================================================
// Code Atelier RPG Maker MZ - Calendar
//=============================================================================

/*:
 * @target MZ
 * @plugindesc Adds a calendar function.
 * @author Zecchan Silverlake
 *
 * @help CAMZCalendar.js
 *
 * This plugin implements calendar system for your game.
 * Formatting date/time/phase:
 * 
 * For example, we are formatting for the date January 7th, 789 5:35 PM
 * 
 * Year
 * yyyy - 0789
 * yyy - 789
 * yy - 89
 * y - 789
 * 
 * Month
 * mmmm - January
 * mmm - Jan
 * mm - 01
 * m - 1
 * 
 * Day
 * dd - 07
 * d - 7
 * DD - Monday
 * D - Mon
 * 
 * Hour
 * hh - 05
 * h - 5
 * HH - 17
 * H - 17
 * 
 * Minute
 * ii - 35
 * i - 35
 * 
 * Second
 * ss - 00
 * s - 0
 * 
 * Phase
 * pp - Evening
 * p - Eve
 * 
 * @command consumeap
 * @text Consume AP
 * @desc Consume Action Point
 * 
 * @arg amount
 * @type number
 * @text Amount
 * @default 1
 * @desc How many AP should be consumed.
 *
 * @command nextphase
 * @text Next Phase
 * @desc Advances the time to the next phase
 * 
 * @command nextday
 * @text Next Day
 * @desc Advances the time to the next day
 *
 * @arg time
 * @type number
 * @text Time/Phase
 * @default
 * @desc After setting the day to 00:00 AM, adds time by this value. When inputting time, prefix it with T. For phase, P.
 * 
 * @command addtime
 * @text Add Time
 * @desc Advances the time by specific value
 *
 * @arg time
 * @type string
 * @text Time
 * @default 0
 * @desc Adds time to the current clock
 * 
 * @command subtime
 * @text Subtract Time
 * @desc Go to the past!
 *
 * @arg time
 * @type string
 * @text Time
 * @default 0
 * @desc Substracts time to the current clock
 * 
 * @param timeStruct
 * @text Calendar Time
 * @type struct<CalendarTimeStruct>
 * @desc Describes how time is handled
 * @default {"mode":"true","phases":"[\"{\\\"longname\\\":\\\"Early-Morning\\\",\\\"shortname\\\":\\\"EMr\\\",\\\"start\\\":\\\"0:00\\\",\\\"end\\\":\\\"5:00\\\",\\\"indoorTint\\\":\\\"{\\\\\\\"red\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"green\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"blue\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"saturation\\\\\\\":\\\\\\\"0\\\\\\\"}\\\",\\\"outdoorTint\\\":\\\"{\\\\\\\"red\\\\\\\":\\\\\\\"-48\\\\\\\",\\\\\\\"green\\\\\\\":\\\\\\\"-48\\\\\\\",\\\\\\\"blue\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"saturation\\\\\\\":\\\\\\\"48\\\\\\\"}\\\"}\",\"{\\\"longname\\\":\\\"Morning\\\",\\\"shortname\\\":\\\"Mor\\\",\\\"start\\\":\\\"5:00\\\",\\\"end\\\":\\\"8:00\\\",\\\"indoorTint\\\":\\\"{\\\\\\\"red\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"green\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"blue\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"saturation\\\\\\\":\\\\\\\"0\\\\\\\"}\\\",\\\"outdoorTint\\\":\\\"{\\\\\\\"red\\\\\\\":\\\\\\\"-16\\\\\\\",\\\\\\\"green\\\\\\\":\\\\\\\"-16\\\\\\\",\\\\\\\"blue\\\\\\\":\\\\\\\"6\\\\\\\",\\\\\\\"saturation\\\\\\\":\\\\\\\"0\\\\\\\"}\\\"}\",\"{\\\"longname\\\":\\\"Noon\\\",\\\"shortname\\\":\\\"Non\\\",\\\"start\\\":\\\"8:00\\\",\\\"end\\\":\\\"16:00\\\",\\\"indoorTint\\\":\\\"{\\\\\\\"red\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"green\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"blue\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"saturation\\\\\\\":\\\\\\\"0\\\\\\\"}\\\",\\\"outdoorTint\\\":\\\"{\\\\\\\"red\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"green\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"blue\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"saturation\\\\\\\":\\\\\\\"0\\\\\\\"}\\\"}\",\"{\\\"longname\\\":\\\"Evening\\\",\\\"shortname\\\":\\\"Eve\\\",\\\"start\\\":\\\"16:00\\\",\\\"end\\\":\\\"19:00\\\",\\\"indoorTint\\\":\\\"{\\\\\\\"red\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"green\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"blue\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"saturation\\\\\\\":\\\\\\\"0\\\\\\\"}\\\",\\\"outdoorTint\\\":\\\"{\\\\\\\"red\\\\\\\":\\\\\\\"38\\\\\\\",\\\\\\\"green\\\\\\\":\\\\\\\"-14\\\\\\\",\\\\\\\"blue\\\\\\\":\\\\\\\"-14\\\\\\\",\\\\\\\"saturation\\\\\\\":\\\\\\\"0\\\\\\\"}\\\"}\",\"{\\\"longname\\\":\\\"Night\\\",\\\"shortname\\\":\\\"Nig\\\",\\\"start\\\":\\\"19:00\\\",\\\"end\\\":\\\"24:00\\\",\\\"indoorTint\\\":\\\"{\\\\\\\"red\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"green\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"blue\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"saturation\\\\\\\":\\\\\\\"0\\\\\\\"}\\\",\\\"outdoorTint\\\":\\\"{\\\\\\\"red\\\\\\\":\\\\\\\"-38\\\\\\\",\\\\\\\"green\\\\\\\":\\\\\\\"-38\\\\\\\",\\\\\\\"blue\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"saturation\\\\\\\":\\\\\\\"48\\\\\\\"}\\\"}\"]","useap":"true","maxap":"5","realtime":"false","rtcscale":"10"}
 * 
 * @param dateStruct
 * @text Calendar Date
 * @type struct<CalendarDateStruct>
 * @desc Describes how date is handled
 * @default {"mode":"true","dows":"[\"{\\\"longname\\\": \\\"Monday\\\", \\\"shortname\\\": \\\"Mon\\\"}\",\"{\\\"longname\\\": \\\"Tuesday\\\", \\\"shortname\\\": \\\"Tue\\\"}\",\"{\\\"longname\\\": \\\"Wednesday\\\", \\\"shortname\\\": \\\"Wed\\\"}\",\"{\\\"longname\\\": \\\"Thursday\\\", \\\"shortname\\\": \\\"Thu\\\"}\",\"{\\\"longname\\\": \\\"Friday\\\", \\\"shortname\\\": \\\"Fri\\\"}\",\"{\\\"longname\\\": \\\"Saturday\\\", \\\"shortname\\\": \\\"Sat\\\"}\",\"{\\\"longname\\\": \\\"Sunday\\\", \\\"shortname\\\": \\\"Sun\\\"}\"]","months":"[\"{\\\"longname\\\":\\\"Spring\\\",\\\"shortname\\\":\\\"Spr\\\",\\\"daysinmonth\\\":\\\"30\\\"}\",\"{\\\"longname\\\":\\\"Summer\\\",\\\"shortname\\\":\\\"Sum\\\",\\\"daysinmonth\\\":\\\"30\\\"}\",\"{\\\"longname\\\":\\\"Autumn\\\",\\\"shortname\\\":\\\"Aut\\\",\\\"daysinmonth\\\":\\\"30\\\"}\",\"{\\\"longname\\\":\\\"Winter\\\",\\\"shortname\\\":\\\"Win\\\",\\\"daysinmonth\\\":\\\"30\\\"}\"]"}
 *
 * @param initPhase
 * @text Initial Phase
 * @type number
 * @desc Initial phase of the day. Starts from 0.
 * @default 0
 * 
 * @param initSecond
 * @text Initial Second
 * @type number
 * @desc Initial second. Starts from 0.
 * @default 0
 * 
 * @param initMinute
 * @text Initial Minute
 * @type number
 * @desc Initial minute. Starts from 0.
 * @default 0
 * 
 * @param initHour
 * @text Initial Hour
 * @type number
 * @desc Initial hour. Starts from 0.
 * @default 0
 * 
 * @param initDOW
 * @text Initial DayOfWeek
 * @type number
 * @desc Initial day of week index.
 * @default 0
 * 
 * @param initDay
 * @text Initial Day
 * @type number
 * @desc Initial day of the calendar. Starts from 1.
 * @default 1
 * 
 * @param initMonth
 * @text Initial Month
 * @type number
 * @desc Initial month of the calendar. Starts from 1.
 * @default 1
 * 
 * @param initYear
 * @text Initial Year
 * @type number
 * @desc Initial year of the calendar. Cannot be zero. Accepts positive and negative.
 * @default 1
 * 
 * @param showMapWindow
 * @text Show Map Window
 * @type boolean
 * @desc Should the DateTime be shown on map?
 * @default false
 * 
 * @param mapTopFormat
 * @text Map Window Top Format
 * @type string
 * @desc What should be shown on the map window top line?
 * @default dd mmm yyyy
 * 
 * @param mapTopPrefix
 * @text Map Window Top Prefix
 * @type string
 * @desc Prefix for the top line
 * @default
 * 
 * @param mapTopSuffix
 * @text Map Window Top Suffix
 * @type string
 * @desc Suffix for the top line
 * @default
 * 
 * @param mapBottomFormat
 * @text Map Window Bottom Format
 * @type string
 * @desc What should be shown on the map window bottom line?
 * @default DD (pp)
 * 
 * @param mapBottomPrefix
 * @text Map Window Bottom Prefix
 * @type string
 * @desc Prefix for the bottom line
 * @default
 * 
 * @param mapBottomSuffix
 * @text Map Window Bottom Suffix
 * @type string
 * @desc Suffix for the bottom line
 * @default
 * 
 * @param resetEveryday
 * @text Reset switch
 * @type struct<ResetSwitchStruct>[]
 * @desc Reset listed switch when the day changes
 * @default
 */

/*~struct~ResetSwitchStruct:
 * @param id
 * @text Switch ID
 * @type number
 * @desc The switch ID to reset 
 *
 * @param value
 * @text Value
 * @type boolean
 * @on On
 * @off Off
 * @desc Set the switch to this value
 * @default false
 * 
 * @param desc
 * @text Description
 * @type string
 * @desc Custom description, does not affect anything
 * @default
*/

/*~struct~CalendarTimeStruct:
 *
 * @param mode
 * @text Mode
 * @type boolean
 * @on Phase
 * @off Clock
 * @desc Phase = use phase of day instead of clock. Clock = use HH:mm:ss
 * @default true
 * 
 * @param phases
 * @text Phases
 * @type struct<CalendarPhaseStruct>[]
 * @desc Phase & Clock: Sets up the phases of day
 * @default
 * 
 * @param useap
 * @text Use Action Point
 * @type boolean
 * @on Yes
 * @off No
 * @desc Phase: When action point is spent, the phase will change.
 * @default true
 * 
 * @param maxap
 * @text Max Action Point
 * @type number
 * @desc Phase: Initial maximum action point per phase
 * @default 5
 * 
 * @param realtime
 * @text Real Time Clock
 * @type boolean
 * @on Use
 * @off Do Not Use
 * @desc Clock: Should the system use real time clock? (experimental)
 * @default false
 * 
 * @param rtcscale
 * @text RTC Scale
 * @type number
 * @desc Clock-RTC: How many in-game second elapsed for every second elapsed in real time.
 * @default 10
 * 
 */

/*~struct~CalendarDateStruct:
 *
 * @param mode
 * @text Mode
 * @type boolean
 * @on Day Only
 * @off DMY
 * @desc Day Only = use day only. DMY = use day, month and year
 * @default true
 * 
 * @param dows
 * @text Day of Week
 * @type struct<CalendarDOWStruct>[]
 * @desc Describes days that happens in a week, zero-based index.
 * @default ["{\"longname\": \"Monday\", \"shortname\": \"Mon\"}","{\"longname\": \"Tuesday\", \"shortname\": \"Tue\"}","{\"longname\": \"Wednesday\", \"shortname\": \"Wed\"}","{\"longname\": \"Thursday\", \"shortname\": \"Thu\"}","{\"longname\": \"Friday\", \"shortname\": \"Fri\"}","{\"longname\": \"Saturday\", \"shortname\": \"Sat\"}","{\"longname\": \"Sunday\", \"shortname\": \"Sun\"}"]
 * 
 * @param months
 * @text Months
 * @type struct<CalendarMonthStruct>[]
 * @desc Describes months that happens in a year, zero-based index.
 * @default ["{\"longname\":\"Spring\",\"shortname\":\"Spr\",\"daysinmonth\":\"30\"}","{\"longname\":\"Summer\",\"shortname\":\"Sum\",\"daysinmonth\":\"30\"}","{\"longname\":\"Autumn\",\"shortname\":\"Aut\",\"daysinmonth\":\"30\"}","{\"longname\":\"Winter\",\"shortname\":\"Win\",\"daysinmonth\":\"30\"}"]
 * 
 */

/*~struct~CalendarMonthStruct:
 *
 * @param longname
 * @text Long Name
 * @type string
 * @desc The long name of the month
 * @default
 * 
 * @param shortname
 * @text Short Name
 * @type string
 * @desc The short name of the month
 * @default
 * 
 * @param daysinmonth
 * @text Days in Month
 * @type number
 * @desc How many days in this month
 * @default 30
 * 
 * @param rainchance
 * @text Chance of Rain
 * @type number
 * @desc Chance of rain in percent
 * @default 0
 * 
 * @param snowchance
 * @text Chance of Snow
 * @type number
 * @desc Chance of snow in percent
 * @default 0
 * 
 */

/*~struct~CalendarDOWStruct:
 *
 * @param longname
 * @text Long Name
 * @type string
 * @desc The long name of the day
 * @default
 * 
 * @param shortname
 * @text Short Name
 * @type string
 * @desc The short name of the day
 * @default
 * 
 */

/*~struct~CalendarPhaseStruct:
 *
 * @param longname
 * @text Long Name
 * @type string
 * @desc The long name of the phase
 * @default
 * 
 * @param shortname
 * @text Short Name
 * @type string
 * @desc The short name of the phase
 * @default
 * 
 * @param start
 * @text Starts At
 * @type string
 * @desc Clock: When this phase start in HH:mm format. (24-hours)
 * @default
 * 
 * @param end
 * @text Ends At
 * @type string
 * @desc Clock: When this phase end in HH:mm format. (24-hours)
 * @default
 * 
 * @param indoorTint
 * @text Indoor Tint
 * @type struct<CalendarTintStruct>
 * @desc Tint that will be applied automatically to maps with <indoor> tag.
 * @default
 * 
 * @param outdoorTint
 * @text Outdoor Tint
 * @type struct<CalendarTintStruct>
 * @desc Tint that will be applied automatically to maps with <outdoor> tag.
 * @default
 * 
 */

/*~struct~CalendarTintStruct:
 *
 * @param red
 * @text Red
 * @type number
 * @desc Red tint
 * @default 0
 * 
 * @param green
 * @text Green
 * @type number
 * @desc Green tint
 * @default 0
 * 
 * @param blue
 * @text Blue
 * @type number
 * @desc Blue tint
 * @default 0
 * 
 * @param saturation
 * @text Saturation
 * @type number
 * @desc Color saturation
 * @default 0
 * 
 */