class CAMV_Calendar {
    constructor() {
        this.TimeSystem = {};
        this.DateSystem = {};
        this.InitialDayOfWeek = 0;
        this.Clock = new CAMV_DateTime(this);
        this.MaximumAP = Number(this.TimeSystem.maxap) || 5;
        this.OnTimeChanged = new EventHandler();
        this.CurrentWeather = "none";
        this.Clock.OnTimeChanged.add(function(changed) {
            this.procChanged(changed);
        }.bind(this));
    }

    phaseCount() {
        return this.TimeSystem.phases.length;
    }

    useAP() {
        return this.TimeSystem.useap == "true";
    }

    maxAP() {
        return this.MaximumAP;
    }

    increaseMaxAP(val) {
        this.MaximumAP += Number(val) || 0;
    }

    decreaseMaxAP(val) {
        this.MaximumAP -= Number(val) || 0;
        if (this.MaximumAP < 1) this.MaximumAP = 1;
    }

    usePhase() {
        return this.TimeSystem.mode == "phase";
    }

    useDay() {
        return this.DateSystem.mode == "day";
    }

    monthCount() {
        return this.DateSystem.months.length;
    }

    monthName(index, long) {
        return long ? this.DateSystem.months[index]?.longname : this.DateSystem.months[index]?.shortname;
    }

    dayOfWeekName(index, long) {
        return long ? this.DateSystem.dows[index]?.longname : this.DateSystem.dows[index]?.shortname;
    }

    phaseName(index, long) {
        return long ? this.TimeSystem.phases[index]?.longname : this.TimeSystem.phases[index]?.shortname;
    }

    phaseIndoorTint(index) {
        var t = this.TimeSystem.phases[index]?.indoorTint;
        if (!t) return [0,0,0,0];
        return  [Number(t.red) || 0, Number(t.green) || 0, Number(t.blue) || 0, Number(t.saturation) || 0];
    }
    phaseOutdoorTint(index) {
        var t = this.TimeSystem.phases[index]?.outdoorTint;
        if (!t) return [0,0,0,0];
        return  [Number(t.red) || 0, Number(t.green) || 0, Number(t.blue) || 0, Number(t.saturation) || 0];
    }

    daysInMonth(mon) {
        mon = mon % this.monthCount();
        if (mon < 0) mon += this.monthCount();
        return Number(this.DateSystem.months[mon].daysinmonth) || 1;
    }

    daysInYear() {
        var sum = 0;
        for(var i = 0; i < this.DateSystem.months.length; i++) {
            var m = this.DateSystem.months[i];
            sum += Number(m.daysinmonth) || 0;
        }
        return sum;
    }

    chanceOfRain(mon) {
        mon = mon % this.monthCount();
        if (mon < 0) mon += this.monthCount();
        return Number(this.DateSystem.months[mon].rainchance) || 0;
    }

    chanceOfSnow(mon) {
        mon = mon % this.monthCount();
        if (mon < 0) mon += this.monthCount();
        return Number(this.DateSystem.months[mon].snowchance) || 0;
    }

    nextDay(tm) {
        var cl = this.Clock.clone();
        var tm = CAMV_TimeSpan.compileStr(typeof tm !== "string" ? "0" : tm);

        this.Clock.Day++;
        this.Clock.Hour = tm?.Hour || 0;
        this.Clock.Minute = tm?.Minute || 0;
        this.Clock.Second = tm?.Second || 0;
        this.Clock.Phase = tm?.Phase || 0;
        this.Clock.ActionPoint = this.maxAP();
        this.Clock.normalize();
        var chg = this.Clock.buildChanged(this.Clock, cl);
        this.Clock.procChanged(chg);
        return chg;
    }

    add() {
        return this.Clock.add.apply(this.Clock, arguments);
    }

    subtract() {
        return this.Clock.subtract.apply(this.Clock, arguments);
    }

    set() {
        return this.Clock.set.apply(this.Clock, arguments);
    }

    consumeAP(val) {
        val = val || 1;
        return this.Clock.consumeAP(val);
    }

    canAct(cost) {
        if (cost == 0 || !this.useAP()) return true;
        if (!(cost <= this.Clock.ActionPoint)) return false;
        return true;
    }

    updateEvalArgs() {
        ca.eval.Args.Now = this.Clock.clone(); 
        ca.eval.Args.Weather = this.CurrentWeather;       
        ca.eval.Args.canAct = function(cost) {
            return this.canAct(cost);
        }.bind(this);
    }

    updateTint() {
        if ($gameScreen) {
            if ($dataMap && $dataMap.meta && $dataMap.meta.indoor)
                $gameScreen.startTint(this.phaseIndoorTint(this.Clock.Phase), 0);
            else if ($dataMap && $dataMap.meta && $dataMap.meta.outdoor)
                $gameScreen.startTint(this.phaseOutdoorTint(this.Clock.Phase), 0);
            else
                $gameScreen.startTint([0,0,0,0], 0);
        }
        this.updateWeather();
    }

    updateWeather() {
        if ($gameScreen) {
            if (!$dataMap || !$dataMap.meta || $dataMap.meta.indoor)
                $gameScreen.changeWeather("none", 0, 0);
            else if ($dataMap && $dataMap.meta && $dataMap.meta.outdoor) {
                if (this.CurrentWeather != "none") {
                    var tint = $gameScreen._tone;
                    tint[0] -= 20;
                    tint[1] -= 20;
                    tint[2] -= 20;
                    tint[3] += 20;
                }
                $gameScreen.changeWeather(this.CurrentWeather, Math.round(Math.random() * 5) + 2, 0);
            }
            else
                $gameScreen.changeWeather("none", 0, 0);
        }
    }

    updateWindow() {
        //SceneManager?._scene?._calendarWindow?.refresh();
    }

    updateFlags(changed) {
        var p = mz.plugins.CAMZ_Calendar[0];
        if (changed.includes("day") && p.resetEveryday) {
            var arr = JSON.parse(p.resetEveryday);
            if (Array.isArray(arr)) {
                for(var i = 0; i < arr.length; i++) {
                    var sw = JSON.parse(arr[i]);
                    var swid = Number(sw.id) || 0;
                    var swval = sw.value == "true" || sw.value == true;
                    if (swid) {
                        mz.s(swid, swval);
                    }
                }
            }
        }
    }

    randomizeWeather(changed) {
        if (changed.includes("phase")) {
            var rand = Math.random() * 100;
            var rain = ca.cal.chanceOfRain(ca.cal.Clock.Month - 1);
            var snow = ca.cal.chanceOfSnow(ca.cal.Clock.Month - 1);
            if (rand < rain) {
                this.CurrentWeather = "rain";
            } else if (rand < snow) {
                this.CurrentWeather = "snow";
            } else {
                this.CurrentWeather = "none";
            }
        }
    }

    procChanged(changed) {
        if (!Array.isArray(changed) || changed.length == 0) return;
        this.OnTimeChanged.trigger([changed], this);
        this.randomizeWeather(changed);
        this.updateTint();
        this.updateEvalArgs();
        this.updateWindow();
        this.updateFlags(changed);
        if (changed.length > 0)
            $gameMap.requestRefresh();
    }

    dateTimeFromString(str) {
        return CAMV_DateTime.compileStr(this, str);
    }
}

class CAMV_TimeSpan {
    constructor() {
        this.Phase = 0;
        this.Second = 0;
        this.Minute = 0;
        this.Hour = 0;
        this.Day = 0;
    }

    static compileArr(arr, cal) {
        var k = {};
        for(var i = 0; i < arr.length; i++) {
            var a = arr[i];
            var spl = a.split(" ", 2);
            if (spl.length == 2) {
                k[spl[1].trim()] = Number(spl[0]) || 0;
            }
        }
        
        var fin = new CAMV_TimeSpan();
        fin.Phase = Number(k.p || k.phase) || 0;
        fin.Second = Number(k.s || k.second) || 0;
        fin.Minute = Number(k.i || k.minute) || 0;
        fin.Hour = Number(k.h || k.hour) || 0;
        fin.Day = Number(k.d || k.day) || 0;

        if (cal)
            fin.normalize(cal);

        return fin;
    }

    static compileStr(str, cal) {
        if (typeof str != "string") throw new Error("Argument must be a string");
        const regex = /^([0-9]+)(T([0-9]+):([0-9]+)(:([0-9]+))?|P([0-9]+))?$/;
        var m = str.match(regex);
        
        var fin = new CAMV_TimeSpan();
        fin.Phase = Number(m[7]) || 0;
        fin.Second = Number(m[6]) || 0;
        fin.Minute = Number(m[4]) || 0;
        fin.Hour = Number(m[3]) || 0;
        fin.Day = Number(m[1]) || 0;

        if (cal)
            fin.normalize(cal);

        return fin;
    }

    normalize(cal) {
        var carry = 0;
        if (cal.usePhase()) {
            while (this.Phase >= cal.phaseCount()) {
                carry++;
                this.Phase -= cal.phaseCount();
            }
            while (this.Phase < 0) {
                carry--;
                this.Phase += cal.phaseCount();
            }
        } else {
            while(this.Second >= 60) {
                carry++;
                this.Second -= 60;
            }
            while(this.Second < 0) {
                carry--;
                this.Second += 60;
            }
            this.Minute += carry;
            carry = 0;

            while(this.Minute >= 60) {
                carry++;
                this.Minute -= 60;
            }
            while(this.Minute < 0) {
                carry--;
                this.Minute += 60;
            }
            this.Hour += carry;
            carry = 0;

            while(this.Hour >= 24) {
                carry++;
                this.Hour -= 24;
            }
            while(this.Hour < 0) {
                carry--;
                this.Hour += 24;
            }
        }
        this.Day += carry;
    }
}

class CAMV_DateTime {
    constructor(cal) {
        if (!cal) throw new Error("Parameter cal must be present");
        this.Calendar = cal;
        this.ActionPoint = 0;
        this.Phase = 0;
        this.DayOfWeek = 0;
        this.Second = 0;
        this.Minute = 0;
        this.Hour = 0;
        this.Day = 1;
        this.Month = 1;
        this.Year = 1;
        this.OnTimeChanged = new EventHandler();
        this.__class = "CAMV_DateTime";
    }

    clone() {
        var c = new CAMV_DateTime(this.Calendar);
        for(var k in this) {
            var tv = this[k];
            var cv = c[k];
            if (typeof tv === "number" && typeof cv === "number")
                c[k] = tv;
        }
        return c;
    }

    procChanged(changed) {
        if (!Array.isArray(changed) || changed.length == 0) return;
        if (changed.includes("phase")) 
            this.restoreAP();
        this.OnTimeChanged.trigger([changed], this);
    }

    consumeAP(val) {
        val = val || 1;
        var cl = this.clone();
        this.ActionPoint -= val;
        this.normalize();
        var changed = this.buildChanged(this, cl);
        this.procChanged(changed);
        return changed;
    }

    restoreAP() {
        this.ActionPoint = this.Calendar.maxAP();
    }

    normalize() {
        var cal = this.Calendar;
        var carry = 0;
        if (cal.useAP()) {
            if (this.ActionPoint <= 0) {
                this.restoreAP();
                this.Phase++;
            }
        }
        if (cal.usePhase()) {
            while (this.Phase >= cal.phaseCount()) {
                carry++;
                this.Phase -= cal.phaseCount();
            }
            while (this.Phase < 0) {
                carry--;
                this.Phase += cal.phaseCount();
            }
        } else {
            while(this.Second >= 60) {
                carry++;
                this.Second -= 60;
            }
            while(this.Second < 0) {
                carry--;
                this.Second += 60;
            }
            this.Minute += carry;
            carry = 0;

            while(this.Minute >= 60) {
                carry++;
                this.Minute -= 60;
            }
            while(this.Minute < 0) {
                carry--;
                this.Minute += 60;
            }
            this.Hour += carry;
            carry = 0;

            while(this.Hour >= 24) {
                carry++;
                this.Hour -= 24;
            }
            while(this.Hour < 0) {
                carry--;
                this.Hour += 24;
            }
        }
        this.Day += carry;
        carry = 0;

        if (!cal.useDay()) {
            while(this.Day > this.Calendar.daysInMonth(this.Month)) {
                this.Day -= this.Calendar.daysInMonth(this.Month - 1);
                this.Month++;
            }

            while(this.Day < 1) {
                this.Day += this.Calendar.daysInMonth(this.Month - 2);
                this.Month--;
            }

            while(this.Month > this.Calendar.monthCount()) {
                this.Month -= this.Calendar.monthCount();
                this.Year++;
            }

            while(this.Month < 1) {
                this.Month += this.Calendar.monthCount();
                this.Year--;
            }
        }

        var totalDays = (this.Year - 1) * this.Calendar.daysInYear();
        for(var i = this.Month - 2; i >= 0; i--) {
            totalDays += this.Calendar.daysInMonth(i);
        }
        totalDays += this.Day;
        this.DayOfWeek = (totalDays + this.Calendar.InitialDayOfWeek) % this.Calendar.DateSystem.dows.length;

        if (!cal.usePhase()) {
            for(var i = 0; i < this.TimeSystem.phases.length; i++) {
                var p = this.TimeSystem.phases[i];
            }
        }        
    }

    toString(format) {
        if (typeof format != "string") format = "";
        format = format || "yyyy-mmm-dd hh:ii";
        var regex = /(yyyy|yyy|yy|y|mmmm|mmm|mm|m|dd|d|DD|D|HH|H|hh|h|ii|i|ss|s|pp|p|a|A|cc|c|CC|C)/g;
        var dt = this;
        return format.replace(regex, function(m) {
            if (m == "yyyy") return ((dt.Year % 10000) + "").padZero(4);
            if (m == "yyy") return ((dt.Year % 1000)+ "").padZero(3);
            if (m == "yy") return ((dt.Year % 100) + "").padZero(2);
            if (m == "y") return (dt.Year + "");
            if (m == "mmmm") return dt.Calendar.monthName(dt.Month - 1, true);
            if (m == "mmm") return dt.Calendar.monthName(dt.Month - 1, false);
            if (m == "mm") return (dt.Month + "").padZero(2);            
            if (m == "m") return (dt.Month + "");
            if (m == "dd") return (dt.Day + "").padZero(2);            
            if (m == "d") return (dt.Day + "");
            if (m == "DD") return dt.Calendar.dayOfWeekName(dt.DayOfWeek, true);
            if (m == "D") return dt.Calendar.dayOfWeekName(dt.DayOfWeek, false);
            if (m == "HH") return (dt.Hour + "").padZero(2);            
            if (m == "H") return (dt.Hour + "");
            if (m == "hh") return ((dt.Hour % 12) + "").padZero(2);            
            if (m == "h") return ((dt.Hour % 12) + "");
            if (m == "ii") return (dt.Minute + "").padZero(2);            
            if (m == "i") return (dt.Minute + "");
            if (m == "ss") return (dt.Second + "").padZero(2);            
            if (m == "s") return (dt.Second + "");
            if (m == "pp") return dt.Calendar.phaseName(dt.Phase, true);
            if (m == "p") return dt.Calendar.phaseName(dt.Phase, false);
            if (m == "a")  return dt.Hour >= 12 ? "pm" : "am";
            if (m == "A")  return dt.Hour >= 12 ? "PM" : "AM";
            if (m == "cc") return (dt.ActionPoint + "").padZero(2);            
            if (m == "c") return (dt.ActionPoint + "");
            if (m == "CC") return (dt.Calendar.maxAP() + "").padZero(2);            
            if (m == "C") return (dt.Calendar.maxAP() + "");
            return m;
        });
    }

    buildChanged(a, b) {
        var changed = [];

        var rDate = this.Calendar.useDay() ? ["Day"] : ["Year", "Month", "Day"];
        var rTime = this.Calendar.usePhase() ? ["Phase"] : ["Hour", "Minute", "Second"];
        var rout = rDate.pushRange(rTime);
        var rChanged = false;
        for(var i = 0; i < rout.length; i++) {
            if (rChanged) {
                changed.push(rout[i].toLowerCase());
                continue;
            }
            if (a[rout[i]] != b[rout[i]]) {
                rChanged = true;
                changed.push(rout[i].toLowerCase());
            }
        }

        return changed;
    }

    add(str) {
        var add = arguments.length > 1 ? CAMV_TimeSpan.compileArr(arguments, this.Calendar) : CAMV_TimeSpan.compileStr(str, this.Calendar);
        if (add.Phase > 0 || add.Day > 0)
            this.ActionPoint = this.Calendar.maxAP();
        var cl = this.clone();
        this.Day += add.Day;
        if (this.Calendar.usePhase())
            this.Phase += add.Phase;
        else {
            this.Hour += add.Hour;
            this.Minute += add.Minute;
            this.Second += add.Second;
        }

        this.normalize();

        var changed = this.buildChanged(this, cl);

        this.procChanged(changed);

        return changed;
    }

    subtract(str) {
        var add = arguments.length > 1 ? CAMV_TimeSpan.compileArr(arguments, this.Calendar) : CAMV_TimeSpan.compileStr(str, this.Calendar);
        if (add.Phase > 0 || add.Day > 0)
            this.ActionPoint = this.Calendar.maxAP();
        var cl = this.clone();
        this.Day -= add.Day;
        if (this.Calendar.usePhase())
            this.Phase -= add.Phase;
        else {
            this.Hour -= add.Hour;
            this.Minute -= add.Minute;
            this.Second -= add.Second;
        }

        this.normalize();

        var changed = this.buildChanged(this, cl);

        this.procChanged(changed);

        return changed;
    }

    set(str) {
        if (typeof str != "string") throw new Error("Argument must be a string");
        const regex = /^(([0-9]+)-([0-9]+)-([0-9]+)|([0-9]+))(T([0-9]+):([0-9]+)(:([0-9]+))?|P([0-9]+))?$/;
        var m = str.match(regex);
        var cl = this.clone();

        if (this.Calendar.useDay()) {
            this.Day = Number(m[5]) || 1;
        } else {
            this.Day = Number(m[4]) || 1;
            this.Month = Number(m[3]) || 1;
            this.Year = Number(m[2]) || 1;
        }
        if (this.Calendar.usePhase()) {
            this.Phase = Number(m[11]);
        } else {
            this.Hour = Number(m[7]);
            this.Minute = Number(m[8]);
            this.Second = Number(m[10]);
        }

        this.normalize();

        var changed = this.buildChanged(this, cl);

        this.procChanged(changed);

        return changed;
    }

    static compileStr(cal, str) {
        var o = new CAMV_DateTime(cal);
        o.set(str);
        return o;
    }

    eq(dt) {
        return this.equal(dt);
    }

    equal(dt) {
        if (typeof dt == "string") dt = CAMV_DateTime.compileStr(this.Calendar, dt);
        if (!dt || typeof dt != "object" || dt.__class != "CAMV_DateTime") return false;
        return dt.Year == this.Year && dt.Month == this.Month && dt.Day == this.Day && dt.Hour == this.Hour && dt.Minute == this.Minute && dt.Second == this.Second && dt.Phase == this.Phase;
    }

    gtr(dt) {
        return this.greaterThan(dt);
    }

    greaterThan(dt) {
        if (typeof dt == "string") dt = CAMV_DateTime.compileStr(this.Calendar, dt);
        if (!dt || typeof dt != "object" || dt.__class != "CAMV_DateTime") return false;
        if (dt.Year < this.Year) return true;
        else if (dt.Year == this.Year) {
            if (dt.Month < this.Month) return true;
            else if (dt.Month == this.Month) {
                if (dt.Day < this.Day) return true;
                else if (dt.Day == this.Day) {
                    if (this.Calendar.usePhase()) {
                        return dt.Phase < this.Phase;
                    } else {
                        if (dt.Hour < this.Hour) return true;
                        else if (dt.Hour == this.Hour) {
                            if (dt.Minute < this.Minute) return true;
                            else if (dt.Minute == this.Minute) {
                                return dt.Second < this.Second;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    geq(dt) {
        return this.greaterThanEqual(dt);
    }

    greaterThanEqual(dt) {
        if (typeof dt == "string") dt = CAMV_DateTime.compileStr(this.Calendar, dt);
        if (!dt || typeof dt != "object" || dt.__class != "CAMV_DateTime") return false;
        return this.equal(dt) || this.greaterThan(dt);
    }

    lss(dt) {
        return this.lessThan(dt);
    }

    lessThan(dt) {
        if (typeof dt == "string") dt = CAMV_DateTime.compileStr(this.Calendar, dt);
        if (!dt || typeof dt != "object" || dt.__class != "CAMV_DateTime") return false;
        if (dt.Year > this.Year) return true;
        else if (dt.Year == this.Year) {
            if (dt.Month > this.Month) return true;
            else if (dt.Month == this.Month) {
                if (dt.Day > this.Day) return true;
                else if (dt.Day == this.Day) {
                    if (this.Calendar.usePhase()) {
                        return dt.Phase > this.Phase;
                    } else {
                        if (dt.Hour > this.Hour) return true;
                        else if (dt.Hour == this.Hour) {
                            if (dt.Minute > this.Minute) return true;
                            else if (dt.Minute == this.Minute) {
                                return dt.Second > this.Second;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    leq(dt) {
        return this.lessThanEqual(dt);
    }

    lessThanEqual(dt) {
        if (typeof dt == "string") dt = CAMV_DateTime.compileStr(this.Calendar, dt);
        if (!dt || typeof dt != "object" || dt.__class != "CAMV_DateTime") return false;
        return this.equal(dt) || this.lessThan(dt);
    }
}