if (ca) {
    ca.chars = new CAMZCharaManager();
    ca.jss.parse(`
    [{
        "d": "fill",
        "class": "event"
    },
    {
        "va": "b",
        "ha": "r",
        "xh" : 550,
        "class": "cutin"
    }]
    `);

    // save/load data
    ca.OnGameSave.add(function(data) {
        data.Chara = ca.chars.list;
    });

    ca.OnGameLoad.add(function(data) {
        if (data.Chara) {
            for(var k in data.Chara) {
                var sd = data.Chara[k];
                var chr = ca.chars.list[k];
                if (chr) {
                    chr.HideName = sd.HideName;
                    chr.ForceOutfit = sd.ForceOutfit;
                    chr.ForceCharacterSprite = sd.ForceCharacterSprite;
                    chr.ForceCharacterSpriteIndex = sd.ForceCharacterSpriteIndex;
                    chr.ForceModifier = sd.ForceModifier;
                    chr.SocialLinkExp = sd.SocialLinkExp || 0;
                    chr.SocialLinkRank = sd.SocialLinkRank || 0;
                    chr.SocialLinkReversed = sd.SocialLinkReversed || chr.SocialLinkReversed;
                    chr.SocialLinkUnlocked = sd.SocialLinkUnlocked || chr.SocialLinkUnlocked;
                    for(var w in sd.Stats) {
                        chr.Stats[w] = sd.Stats[w];
                    }
                }
            }
        }
        ca.chars.updateEvalArgs();
    });

    ca.OnMapStart.add(function() {
        ca.chars.updateEvalArgs();
    });

    // Transclurent
    ca.OnGameMessageSetSpeakerName.add(function(name) {
        if (typeof name !== "string" || !ca.chars._transclurent) return;

        // detect char id
        var m = name.match(/\@(\w+)/g);
        var undimmed = null;
        if (m && m[0]) {
            var id = m[0].substr(1);
            if (ca.chars.list[id]) {
                undimmed = id;
            }
        }

        // dim everything except undimmed
        for(var k in ca.chars._shownCutins) {
            var arr = ca.chars._shownCutins[k];
            for(var i = 0; i < arr.length; i++) {
                if ($gameScreen._pictures[arr[i]])
                    $gameScreen._pictures[arr[i]]._opacity = !name || k == undimmed ? 255 : 150;
            }
        }
    });

    // Add escape char
    ca.OnConvertEscapeCharacters.addBefore(function(text) {
        text = text.replace(/\@(\w+)/g, function(match) {
            var id = match.substr(1);
            var chr = ca.chars.list[id];
            if (chr) {
                return chr.getName();
            }
            return match;
        });

        return text;
    }.bind(this));

    // NPC Bind outfit
    ca.OnCheckEventPage.addAfter(function(page) {
        if (this._lastReturn) {
            var c = page.list.where(c => c.code == 357 && c.parameters[0] == "CAMZ_Chara" && c.parameters[1] == "bindchar").first();

            if (c) {
                var cid = c.parameters[3].charid;
                var chr = ca.chars.list[cid];
                var spr = chr.currentCharacterSprite();
                var spridx = chr.currentCharacterSpriteIndex();
                if (spr && spridx) {
                    page.image.characterName = spr;
                    page.image.characterIndex = spridx;
                }
            }
        }
        return this._lastReturn;
    }.bind(ca.OnCheckEventPage));

    // Register to PluginManager
    PluginManager.registerCommand("CAMZ_Chara", "cutin", args => {
        args.index = Number(args.index);    
        ca.chars.showCutin(args);
    });
    PluginManager.registerCommand("CAMZ_Chara", "hidecutin", args => {
        ca.chars.hideCutin(String(args.charid));
    });
    PluginManager.registerCommand("CAMZ_Chara", "event", args => {
        args.index = Number(args.index);    
        ca.chars.showEvent(args);
    });
    PluginManager.registerCommand("CAMZ_Chara", "hideevent", args => {
        ca.chars.hideEvent();
    });
    PluginManager.registerCommand("CAMZ_Chara", "revealname", args => {
        ca.chars.revealName(String(args.charid));
    });
    PluginManager.registerCommand("CAMZ_Chara", "setoutfit", args => {
        ca.chars.forceOutfit(String(args.charid), String(args.outfit));
    });
    PluginManager.registerCommand("CAMZ_Chara", "increasestat", args => {
        var chr = ca.chars.list[args.charid];
        if (chr) {
            chr.stat(args.stat, Number(args.value) || 0, "+");
            mz.refreshEvents();
        }
    });
    PluginManager.registerCommand("CAMZ_Chara", "decreasestat", args => {
        var chr = ca.chars.list[args.charid];
        if (chr) {
            chr.stat(args.stat, Number(args.value) || 0, "-");
            mz.refreshEvents();
        }
    });
    PluginManager.registerCommand("CAMZ_Chara", "expsl", args => {
        var chr = ca.chars.list[args.charid];
        if (chr) {
            chr.gainSLExp(args.exp);
            mz.refreshEvents();
        }
    });
    PluginManager.registerCommand("CAMZ_Chara", "revsl", args => {
        var chr = ca.chars.list[args.charid];
        if (chr) {
            chr.reverseSL(args.value);
            mz.refreshEvents();
        }
    });
    PluginManager.registerCommand("CAMZ_Chara", "unlocksl", args => {
        var chr = ca.chars.list[args.charid];
        if (chr) {
            chr.unlockSL(args.rank);
            mz.refreshEvents();
        }
    });

    // auto read char data
    if (mz.plugins.CAMZ_Chara[0]) {
        var ins = JSON.parse(mz.plugins.CAMZ_Chara[0].initstats);
        var fino = {};
        for(var i = 0; i < ins.length; i++) {
            var o = JSON.parse(ins[i]);
            fino[o.id] = Number(o.value) || 0;
        }
        ca.chars.InitialStats = fino;
        ca.chars._transclurent = mz.plugins.CAMZ_Chara[0].dimchar == "true";
        var a = JSON.parse(mz.plugins.CAMZ_Chara[0].slExp);
        ca.chars._socialLinkRankExp =[]
        for(var i = 0; i < a.length; i++) ca.chars._socialLinkRankExp.push(Number(a[i]) || 0);

        var db = JSON.parse(mz.plugins.CAMZ_Chara[0].chardb);
        for(var i = 0; i < db.length; i++) {
            try{
                var cdt = JSON.parse(db[i]);
                cdt.initStats = JSON.parse(cdt.initStats || "[]");
                for(var w = 0; w < cdt.initStats.length; w++) {
                    cdt.initStats[w] = JSON.parse(cdt.initStats[w]);
                }
                ca.chars.add(cdt);
            }
            catch(error){
                console.error(error);
            }
        }
    }

    console.log("CAMZChara initialized");
}