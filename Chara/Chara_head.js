//=============================================================================
// Code Atelier RPG Maker MZ - Characters
//=============================================================================

/*:
 * @target MZ
 * @plugindesc Allows you to record character data.
 * @author Zecchan Silverlake
 *
 * @help CAMZChara.js
 *
 * This plugin contains necessary logic for CAMZ Character Manager.
 * JavaScript is recommended if you have a lot of characters.
 *
 * @command cutin
 * @text Show Standing Picture
 * @desc Show a character's standing picture
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID to get the standing picture
 *
 * @arg path
 * @type string
 * @text Picture Path 
 * @desc Picture path relative to character's directory.
 * {o} - Will be replaced with current outfit
 *
 * @arg class
 * @type string
 * @text JSS Class 
 * @desc JSS classes to apply, separated by space
 *
 * @arg style
 * @type multiline_string
 * @text Display Style 
 * @desc JSS script
 *
 * @arg index
 * @type number
 * @text Picture Index
 * @default 0
 * @desc Picture index of the stand. Set to 0 for auto.
 *
 * @command hidecutin
 * @text Hide Standing Picture
 * @desc Hide a character's standing picture
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID to get the standing picture. You can empty this field to clear all standing pictures.
 * 
 * @command event
 * @text Show Event Picture
 * @desc Show a character's event picture
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID to get the standing picture
 *
 * @arg path
 * @type string
 * @text Picture Path 
 * @desc Picture path relative to character's directory.
 * {o} - Will be replaced with current outfit
 *
 * @arg class
 * @type string
 * @text JSS Class 
 * @desc JSS classes to apply, separated by space
 *
 * @arg style
 * @type multiline_string
 * @text Display Style 
 * @desc JSS script
 *
 * @arg index
 * @type number
 * @text Picture Index
 * @default 0
 * @desc Picture index of the event. Set to 0 for auto.
 *
 * @command hideevent
 * @text Hide Event Pictures
 * @desc Hide all event pictures
 *
 * @command revealname
 * @text Reveal Name
 * @desc Reveal character's name if hidden
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID to reveal the name.
 *
 * @command setoutfit
 * @text Set Outfit
 * @desc Set Outfit for a character
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID to set the outfit.
 *
 * @arg outfit
 * @type string
 * @text Outfit
 * @desc The outfit name, set blank for auto.
 *
 * @command increasestat
 * @text Increase Stat
 * @desc Increase Stat for a character
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID.
 *
 * @arg stat
 * @type string
 * @text Stat
 * @desc The stat ID.
 *
 * @arg value
 * @type number
 * @text Value
 * @desc The value to add to the stat.
 *
 * @command decreasestat
 * @text Decrease Stat
 * @desc Decrease Stat for a character
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character ID.
 *
 * @arg stat
 * @type string
 * @text Stat
 * @desc The stat ID.
 *
 * @arg value
 * @type number
 * @text Value
 * @desc The value to subtract from the stat.
 * 
 * @command bindchar
 * @text Bind Character
 * @desc Binds character outfit to this event
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc This event will follow this character sprite.
 * 
 * @param chardb
 * @text Character Database
 * @type struct<CharaStruct>[]
 * @default []
 * 
 * @param initstats
 * @text Initial Stats
 * @type struct<CharaStatStruct>[]
 * @default []
 * 
 * @param dimchar
 * @text Transclurent
 * @type boolean
 * @on Use
 * @off Do not Use
 * @default false
 * @desc Makes character opaque/transclurent automatically by detecting message boxes' Name section.
 *
 * @param slExp
 * @type number[]
 * @text Social Link Experience
 * @desc The total exp needed to rank up Social Link. (starts from Rank 1, but Rank 1 will automatically gained from unlocking)
 * @default [0,3,5,8,12,17,22,27,33,40]
 * 
 * @command unlocksl
 * @text Unlock Social Link
 * @desc Unlocks a character social link
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character to modify the SL.
 *
 * @arg rank
 * @type number
 * @text Rank
 * @desc Set the rank to this value after unlocking (min: 1)
 * @default 1
 * 
 * @command expsl
 * @text Social Link Experience
 * @desc Gain specified amount of SL Experience
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character to modify the SL.
 *
 * @arg exp
 * @type number
 * @text Experience
 * @desc The experience to gain
 * @default 1
 * 
 * @command revsl
 * @text Social Link Reverse
 * @desc Set the reverse status of the social link
 *
 * @arg charid
 * @type string
 * @text Character ID
 * @desc The character to modify the SL.
 *
 * @arg value
 * @type boolean
 * @text Reverse
 * @on Yes
 * @off No
 * @desc Set to "No", to undo the effect
 * @default true
 */


/*~struct~CharaStruct:
 *
 * @param id
 * @text ID
 * @type string
 * @desc The ID of the character. Only alphanumeric and no space, it must start with alphabet.
 * @default
 * 
 * @param name
 * @text Name
 * @type string
 * @desc The Name of the character. This is what will be displayed as the character name. Use @name in dialogues.
 * @default
 * 
 * @param hideName
 * @text Hide Name
 * @type boolean
 * @desc When set to hide, the character name will shown as ??? before revealed.
 * @on Hide
 * @off Show
 * @default false
 * 
 * @param color
 * @text Color
 * @type string
 * @desc Automatically color character name if shown using @name.
 * @default white
 * 
 * @param actor
 * @text Actor ID
 * @type number
 * @desc Bind this character to an actor. blank = unbind (Affects outfit and images based on equipment)
 * @default
 * 
 * @param eventPath
 * @text Base Event Path
 * @type string
 * @desc Base path for event images.
 * @default {id}/evt
 * 
 * @param cutinPath
 * @text Base Cutin Path
 * @type string
 * @desc Base path for cutin images.
 * @default {id}/ctn
 * 
 * @param defaultOutfit
 * @text Default Outfit
 * @type string
 * @desc The default value for outfit. {o}
 * @default default
 * 
 * @param defaultCharacterSprite
 * @text Default Sprite
 * @type string
 * @desc The default sprite for the character.
 * @default
 * 
 * @param defaultCharacterSpriteIndex
 * @text Default Sprite Index
 * @type string
 * @desc The default sprite index for the character.
 * @default
 * 
 * @param defaultModifier
 * @text Default Modifier
 * @type string
 * @desc The default modifier for the character. {m}
 * @default
 * 
 * @param initStats
 * @text Initial Stats
 * @type struct<CharaStatStruct>[]
 * @desc The initial stats for the character. Missing value will use default.
 * @default
 * 
 * @param slLabel
 * @text Social Link Label
 * @type string
 * @desc Label of the social link. Example: The Fool
 * @default
 *
 * @param ctnstyle
 * @type multiline_string
 * @text Cutin Display Style 
 * @desc JSS script
 *
 * @param evtstyle
 * @type multiline_string
 * @text Event Display Style 
 * @desc JSS script
 * 
 */

/*~struct~CharaStatStruct:
 *
 * @param id
 * @text ID
 * @type string
 * @desc The stat id.
 * @default
 * 
 * @param value
 * @text Value
 * @type number
 * @desc The value of the stat.
 * @default 0
 * 
 */