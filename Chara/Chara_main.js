 class CAMZChara {
    constructor(opt) {
        this.Id = opt.id || "";
        this.Name = opt.name || "";
        this.HideName = opt.hideName === "true" || opt.hideName === true;
        this.Color = opt.color || "white";
        this.Actor = opt.actor || null;

        this.EventPath = (opt.eventPath || "{id}/evt") + "/";
        this.CutinPath = (opt.cutinPath || "{id}/ctn") + "/";

        this.DefaultOutfit = opt.defaultOutfit || "default";
        this.ForceOutfit = null;
        this.DefaultCharacterSprite = opt.defaultCharacterSprite || null;
        this.ForceCharacterSprite = null;
        this.DefaultCharacterSpriteIndex = opt.defaultCharacterSpriteIndex || null;
        this.ForceCharacterSpriteIndex = null;
        this.DefaultModifier = opt.defaultModifier || null;
        this.ForceModifier = null;

        this.CutinStyle = opt.ctnstyle;
        this.EventStyle = opt.evtstyle;

        this.SocialLinkExp = 0;
        this.SocialLinkRank = 0;
        this.SocialLinkReversed = false;
        this.SocialLinkUnlocked = false;
        this.SocialLinkLabel = opt.slLabel;

        this.Stats = {};
        if (typeof opt.stats === "object" && opt.stats) {
            for(var k in opt.stats) this.Stats[k] = opt.stats;
        }
        if (Array.isArray(opt.initStats)) {
            for(var i = 0; i < opt.initStats.length; i++) {
                var x = opt.initStats[i];
                if (typeof x == "object") {
                    this.Stats[x.id] = Number(x.value) || 0;
                }
            }
        }
        for(var k in ca.chars.InitialStats) {
            if (this.Stats[k] === undefined) this.Stats[k] = ca.chars.InitialStats[k];
        }
    }

    getName() {
        return "\\c[" + this.Color + "]" + (this.HideName ? "???" : this.Name) + "\\c[0]";
    }

    getEquipMeta() {
        var meta = {
            outfit: null,
            characterSprite: null,
            characterSpriteIndex: null,
            modifier: null
        };
        if (this.Actor) {
            var eq = $gameActors.actor(this.Actor).equips();
            for(var i = 0; i < eq.length; i++) {
                var eqd = eq[i];
                if (eqd && eqd.meta) {
                    if (eqd.meta.outfit) meta.outfit = eqd.meta.outfit;
                    if (eqd.meta["sprite[" + this.Id + "]"]) {
                        var spr = eqd.meta["sprite[" + this.Id + "]"].trim().split("#");
                        meta.characterSprite = spr[0].trim();
                        meta.characterSpriteIndex = Number(spr[1]) || 0;
                    }
                    if (eqd.meta.modifier) meta.modifier = eqd.meta.modifier;
                }
            }
        }
        return meta;
    }

    currentOutfit() {
        var meta = this.getEquipMeta();
        return this.ForceOutfit || meta.outfit || this.DefaultOutfit;
    }

    currentCharacterSprite() {
        var meta = this.getEquipMeta();
        return this.ForceCharacterSprite || meta.characterSprite || this.DefaultCharacterSprite;
    }

    currentCharacterSpriteIndex() {
        var meta = this.getEquipMeta();
        return this.ForceCharacterSpriteIndex || meta.characterSpriteIndex || this.DefaultCharacterSpriteIndex;
    }

    currentModifier() {
        var meta = this.getEquipMeta();
        return this.ForceModifier || meta.modifier || this.DefaultModifier;
    }

    forceOutfit(outfit) {
        this.ForceOutfit = outfit;
    }

    forceSprite(sprite, index) {
        this.ForceCharacterSprite = sprite;
        this.ForceCharacterSpriteIndex = index;
        mz.refreshEvents();
        mz.refreshPlayer();
    }

    getCutinImage(path) {
        path = this.CutinPath + path;
        path = path.replace("{o}", this.currentOutfit());
        path = path.replace("{m}", this.currentModifier());
        path = path.replace("{id}", this.Id);
        return path;
    }

    getEventImage(path) {
        path = this.EventPath + path;
        path = path.replace("{o}", this.currentOutfit());
        path = path.replace("{m}", this.currentModifier());
        path = path.replace("{id}", this.Id);
        return path;
    }

    stat(key, val, valmode) {
        valmode = valmode || "set";
        if(this.Stats[key] === undefined) return;
        if (val) {
            if (valmode == "add" || valmode == "+")
                this.Stats[key] += val;
            else if (valmode == "subtract" || valmode == "-")
                this.Stats[key] -= val;
            else
                this.Stats[key] = val;
        }
        ca.chars.updateEvalArgs();
        mz.refreshEvents();
        return this.Stats[key];
    }

    gainSLExp(val) {
        if (!this.SocialLinkUnlocked) return;
        if (this.SocialLinkReversed) return;
        var num = Number(val) || 0;
        if (!num) return;
        this.SocialLinkExp += num;
    }

    reverseSL(val) {
        this.SocialLinkReversed = val == "true" || val == true;
    }

    unlockSL(val) {
        var num = Number(val) || 0;
        this.SocialLinkUnlocked = true;
        this.SocialLinkExp = 0;
        this.SocialLinkRank = 1;
        if (!num) return;
        this.SocialLinkExp = ca.chars._socialLinkRankExp[num - 1] || 0;
        this.SocialLinkRank = num;
    }
 }
