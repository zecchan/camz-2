 class CAMZCharaManager {
    constructor() {
        this.InitialStats = {};
        this.list = {};
        this._shownEvents = [];
        this._shownCutins = {};
        this._transclurent = false;
    }

    add(opt) {
        var c = new CAMZChara(opt);
        if (c.Id) this.list[c.Id] = c;
        return this;
    }

    updateEvalArgs() {
        ca.eval.Args.char = {};
        for(var k in this.list) {
            var o = {};
            var s = this.list[k].Stats;
            for(var st in s) {
                o[st] = s[st];
            }
            o.sl = {
                exp: this.list[k].SocialLinkExp,
                rank: this.list[k].SocialLinkRank,
                reversed: this.list[k].SocialLinkReversed,
                unlocked: this.list[k].SocialLinkUnlocked,
                canRankUp: ca.chars._socialLinkRankExp[this.list[k].SocialLinkRank] < this.list[k].SocialLinkExp && !this.list[k].SocialLinkReversed && this.list[k].SocialLinkUnlocked
            };
            ca.eval.Args.char[k] = o;
        }
    }

    getNewCutinIndex() {
        var mx = 49;
        for(var k in this._shownCutins) {
            if (Array.isArray(this._shownCutins[k])) {
                mx = Math.max(this._shownCutins[k].max() || 0, mx);
            } else {
                mx = Math.max(Number(this._shownCutins[k]) || 0, mx);
            }
        }
        return mx + 1;
    }

    showCutin(opt) {
        this.hideCutin(opt.charid);
        var char = this.list[opt.charid];
        if (!char) return;
        var ix = Number(opt.index) || this.getNewCutinIndex();

        var paths = opt.path.split(",");
        var ids = [];
        for(var i = 0; i < paths.length; i++) {
            var fcls = ["cutin"];
            var cls = typeof opt.class == "string" ? opt.class.split(" ").where(x => x) : [];
            fcls.pushRange(cls);
            ca.img.showPicture(ix, char.getCutinImage(paths[i].trim()), fcls, opt.style || char.CutinStyle || undefined);
            ids.push(ix++);
        }
        this._shownCutins[char.Id] = ids;
    }

    hideCutin(id) {
        if (!id) {
            for(var k in this._shownCutins) {
                ca.img.clearPicture(this._shownCutins[k]);
                delete this._shownCutins[k];
            }
        } else {
            if (this._shownCutins[id]) {
                ca.img.clearPicture(this._shownCutins[id]);
                delete this._shownCutins[id];
            }
        }
    }

    showEvent(opt) {
        this.hideEvent();
        var char = this.list[opt.charid];
        if (!char) return;
        var ix = Number(opt.index) || 10;

        var paths = opt.path.split(",");
        var ids = [];
        for(var i = 0; i < paths.length; i++) {
            var fcls = ["event"];
            var cls = typeof opt.class == "string" ? opt.class.split(" ").where(x => x) : [];
            fcls.pushRange(cls);
            ca.img.showPicture(ix, char.getEventImage(paths[i].trim()), fcls, opt.style || char.EventStyle || undefined);
            ids.push(ix++);
        }
        this._shownEvents = ids;
    }

    hideEvent() {
        for(var i = 0; i < this._shownEvents.length; i++) {
            ca.img.clearPicture(this._shownEvents[i]);
        }
        this._shownEvents = [];
    }

    revealName(id) {
        if (this.list[id])
            this.list[id].HideName = false;
    }

    forceOutfit(id, outfit) {
        if (this.list[id])
            this.list[id].forceOutfit(outfit || null);
    }

    forceSprite(id, sprite, index) {
        if (this.list[id])
            this.list[id].forceSprite(sprite || null, index || null);
    }
 }
